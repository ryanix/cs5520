package edu.neu.madcourse.xingxingliu.Scraggle;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import edu.neu.madcourse.xingxingliu.TwoPlayer.TwoPlayerGameActivity;
import edu.neu.madcourse.xingxingliu.TwoPlayer.TwoPlayerGameFragment;

public class WordTile {

    private String word="";
    private ArrayList<WordTile> pressedChild = new ArrayList<>();

    public void setWord(String letter){
        this.word=word+letter;
    }
    public void clearWord(){
        this.word="";
    }

    public String getWord(){
        return this.word;
    }

    public void addPressedChild(WordTile smallTile){
        this.pressedChild.add(smallTile);
    }
    public ArrayList<WordTile> getPressedChild(){
        return this.pressedChild;
    }
    public void clearPressedChild(){
        this.pressedChild.clear();
    }


    private final WordGameFragment mGame;
    private final TwoPlayerGameFragment mTPGame;
    private View mView;
    private WordTile mSubWordTiles[];
    private boolean validStatus=false;

    public void setValidStatus(Boolean b)
    {
        this.validStatus=b;
    }
    public Boolean getValidStatus(){
        return validStatus;
    }

    public WordTile(TwoPlayerGameFragment game){
        this.mTPGame = game;
        mGame = null;
    }
    public WordTile(WordGameFragment game) {
        this.mGame = game;
        mTPGame = null;
    }

    public View getView() {
        return mView;
    }

    public void setView(View view) {
        this.mView = view;
    }

    public WordTile[] getSubWordTiles() {
        return mSubWordTiles;
    }

    public void setSubWordTiles(WordTile[] subWordTiles) {
        this.mSubWordTiles = subWordTiles;
    }

    public void animate() {
        Animator anim = AnimatorInflater.loadAnimator(mGame.getActivity(),
                org.example.tictactoe.R.animator.tictactoe);
        if (getView() != null) {
            anim.setTarget(getView());
            anim.start();
        }
    }
}
