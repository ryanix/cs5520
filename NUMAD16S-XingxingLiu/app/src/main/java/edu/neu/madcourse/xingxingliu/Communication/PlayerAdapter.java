package edu.neu.madcourse.xingxingliu.Communication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.xingxingliu.TwoPlayer.TwoPlayerCommunicate;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by Ryanix on 3/10/16.
 */
public class PlayerAdapter extends BaseAdapter implements View.OnClickListener {
    private Activity activity;
    private Activity pointActivity;
    private ArrayList<Player> data;
    private static LayoutInflater inflater = null;
    Player tempValues = null;

    public PlayerAdapter(Activity a, ArrayList<Player> l) {
        activity = a;
        data = l;
        inflater = (LayoutInflater) activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /********
     * What is the size of Passed Arraylist Size
     ************/
    public int getCount() {

        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /*********
     * Create a holder Class to contain inflated xml file elements
     *********/
    public static class ViewHolder {

        public TextView playerName;
        public TextView playerScore;
        public TextView playerId;
        public TextView scorePrefix;
    }

    /******
     * Depends upon data size called for each row , Create each ListView row
     *****/
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.player_info, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.playerId = (TextView) vi.findViewById(R.id.playerId);
            holder.playerName = (TextView) vi.findViewById(R.id.playerName);
            holder.playerScore = (TextView) vi.findViewById(R.id.playerScore);
            holder.scorePrefix = (TextView) vi.findViewById(R.id.scorePrefix);

            /************  Set holder with LayoutInflater ************/
            vi.setTag(holder);
        } else
            holder = (ViewHolder) vi.getTag();

        if (data.size() <= 0) {
            holder.playerName.setText("No Player Available");
            holder.scorePrefix.setText("");

        } else {
            /***** Get each Model object from Arraylist ********/
            tempValues = null;
            tempValues = (Player) data.get(position);

            /************  Set Model values in Holder elements ***********/

            holder.playerId.setText(tempValues.getId());
            holder.playerScore.setText(tempValues.getScore().toString());
            holder.playerName.setText(tempValues.getUsername());

            /******** Set Item Click Listner for LayoutInflater for each row *******/

            vi.setOnClickListener(new OnItemClickListener(position));
        }
        return vi;
    }


    @Override
    public void onClick(View view) {

    }

    /*********
     * Called when Item click in ListView
     ************/
    private class OnItemClickListener implements View.OnClickListener {
        private int mPosition;

        OnItemClickListener(int position) {
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {
            if (activity instanceof CommunicationNamingActivity) {
                CommunicationNamingActivity sct = (CommunicationNamingActivity) activity;
                sct.onItemClick(mPosition);

            }
            else if(activity instanceof TwoPlayerCommunicate) {
                TwoPlayerCommunicate sct = (TwoPlayerCommunicate) activity;
                sct.onItemClick(mPosition);

            }

            /****  Call  onItemClick Method inside CustomListViewAndroidExample Class ( See Below )****/

        }
    }
}