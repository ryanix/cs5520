package edu.neu.madcourse.xingxingliu.TwoPlayer;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.neu.madcourse.xingxingliu.Communication.CommunicationConstants;
import edu.neu.madcourse.xingxingliu.Communication.GcmNotification;
import edu.neu.madcourse.xingxingliu.Communication.Player;
import edu.neu.madcourse.xingxingliu.Communication.PlayerAdapter;
import edu.neu.madcourse.xingxingliu.Communication.RemoteClient;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.MyDatabase;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by Ryanix on 3/20/16.
 */
public class TwoPlayerCommunicate extends AppCompatActivity implements OnClickListener {
    private static final String FIREBASE_DB = "https://luminous-fire-4892.firebaseIO.com";
    private Button confirm;
    private EditText inputUsername;
    private Button showContent;
    private TextView title;
    private TableRow registration;
    private ListView playersInfo;
    PlayerAdapter adapter;
    public TwoPlayerCommunicate CustomListView = null;
    public ArrayList<Player> players = new ArrayList<Player>();

    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERT_REG_NAME = "registration_name";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    static final String TAG = "GCM Sample Demo";
    GoogleCloudMessaging gcm;
    Context context;
    private static String regid;
    private static String regname;
    private AlertDialog mDialog;
    private String[] wordList = new String[9];
    private MyDatabase myDatabase;
    private SQLiteDatabase db;
    private WakefulBroadcastReceiver receiver;
    private IntentFilter filter;
    private String teammateId;
    private String teammateName;
    private int myScore;
    private ProgressDialog progressBar;
    private String wordlist = "";

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        context = getApplicationContext();
        SharedPreferences pref = getSharedPreferences(TwoPlayerCommunicate.class.getSimpleName(), MODE_PRIVATE);
        regid = pref.getString(PROPERTY_REG_ID, "");
        regname = pref.getString(PROPERT_REG_NAME, "");
        setReceiver();
        getWordList();
        getSupportActionBar().setTitle("Two Player");
        setContentView(R.layout.communication_registration);
        showContent = (Button) findViewById(R.id.show_content);
        inputUsername = (EditText) findViewById(R.id.username_input);
        registration = (TableRow) findViewById(R.id.registration_view);
        title = (TextView) findViewById(R.id.title_for_registration);
        playersInfo = (ListView) findViewById(R.id.playerList);
        confirm = (Button) findViewById(R.id.registration_confirm);
        gcm = GoogleCloudMessaging.getInstance(this);
        CustomListView = this;
        progressBar = new ProgressDialog(this);
        showContent.setVisibility(View.INVISIBLE);
        playersInfo.setVisibility(View.INVISIBLE);
        if (regid != "") {
            title.setText("Players List");
            registration.setVisibility(View.GONE);
            showContent.setVisibility(View.VISIBLE);
            playersInfo.setVisibility(View.VISIBLE);
        }
        fillPlayerList();
    }

    @SuppressLint("NewApi")
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);

        return registrationId;
    }

    public Boolean checkUsername(String username) {
        for (Player p : players) {
            if (p.getUsername() == username)
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view == inputUsername) {
            inputUsername.setText("");
        } else if (view == showContent) {
            recreate();
        } else if (view == confirm) {
            String input = inputUsername.getText().toString();
            if (inputUsername == null) {
                Toast.makeText(context, "Name can not be empty", Toast.LENGTH_LONG).show();
            } else if (checkUsername(input)) {
                Toast.makeText(context, "Name already exists", Toast.LENGTH_LONG).show();
                inputUsername.setText("");
            } else {
                if (checkPlayServices()) {
                    Toast.makeText(context, "Registering~!", Toast.LENGTH_LONG).show();

                    registerInBackground(input);
                }
            }
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        9000).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(TwoPlayerCommunicate.class.getSimpleName(), Context.MODE_PRIVATE);
    }


    private void registerInBackground(final String username) {
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(CommunicationConstants.GCM_SENDER_ID);
                    msg = "Device registered";
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage() + " Please try again";
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == "Device registered") {

                    regname = username;
                    Player player = new Player(regname, regid, 0);
                    RemoteClient remoteClient = new RemoteClient(context);
                    remoteClient.saveValue("Player", regname, player);
                    storeRegistrationId(context, regid, regname);

                    showContent.setVisibility(View.VISIBLE);
                    playersInfo.setVisibility(View.VISIBLE);
                    registration.setVisibility(View.GONE);
                    title.setText("Players List");

                }
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        }.execute(username);
    }

    private void storeRegistrationId(Context context, String regId, String regName) {
        regid = regId;
        regname = regName;
        final SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putString(PROPERT_REG_NAME, regName);
        editor.commit();
    }

    public void fillPlayerList() {
        Firebase.setAndroidContext(context);
        Firebase ref = new Firebase(FIREBASE_DB + "/" + "Player");
        Query queryRef = ref.orderByChild("score");
        RemoteClient remoteClient = new RemoteClient(context);
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // snapshot contains the key and value
                if (snapshot.getValue() != null) {
                    Log.d(TAG, "Data Received" + snapshot.getValue().toString());

                    // Adding the data to the ArrayList
                    for (DataSnapshot postshot : snapshot.getChildren()) {
                        Player player = postshot.getValue(Player.class);
                        String name = player.getUsername();
                        if (!name.equals(regname)) {
                            players.add(player);
                        } else {
                            myScore = player.getScore();
                        }
                    }
                } else {
                    Log.d(TAG, "Data Not Received");
                }
                PlayerAdapter adapter = new PlayerAdapter(CustomListView, players);
                playersInfo.setAdapter(adapter);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(TAG, firebaseError.getMessage());
                Log.e(TAG, firebaseError.getDetails());
            }
        });
        players.addAll(remoteClient.getPlayerInfo());

    }


    public void onItemClick(Integer mPostion) {
        Player player = players.get(mPostion);
        teammateName = player.getUsername();
        teammateId = player.getId();
        sendMessage(teammateId, regname, wordlist, regid);
        waitForConnection();
    }

    private void waitForConnection() {
        progressBar.setCancelable(false);
        progressBar.setMessage("Waiting For " + teammateName + " To Connect");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.show();
        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (progressBar.isShowing())
                    progressBar.dismiss();
            }
        }.start();


    }

    private void startGame(String vsId, String vsName, String wordlist) {
        Intent intent = new Intent(this, TwoPlayerGameActivity.class);
        intent.putExtra("vsId", vsId);
        intent.putExtra("vsName", vsName);
        intent.putExtra("word", wordlist);
        intent.putExtra("myId", regid);
        intent.putExtra("myName", regname);
        intent.putExtra("myScore", myScore);
        startActivity(intent);
    }

    private void setReceiver() {
        receiver = new WakefulBroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                Bundle extra = intent.getExtras();
                if (!extra.isEmpty()) {
                    final String vsId = extra.getString("Id");
                    final String vsName = extra.getString("teammateName");
                    final String words = extra.getString("word");
                    if (words != null) {
                        if (!words.equals("start")) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TwoPlayerCommunicate.this);
                            builder.setTitle("Connecting Request From " + vsName);
                            builder.setCancelable(true);
                            builder.setPositiveButton("Connect",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            sendMessage(vsId, regname, "start", regid);
                                            startGame(vsId, vsName, words);
                                        }
                                    });
                            mDialog = builder.show();
                            new CountDownTimer(5000, 1000) {
                                @Override
                                public void onTick(long millisUntilFinished) {

                                }

                                @Override
                                public void onFinish() {
                                    mDialog.dismiss();
                                }
                            }.start();
                        }
                        else if(words.equals("start")){
                            if (progressBar.isShowing())
                                progressBar.dismiss();
                            startGame(teammateId, teammateName, wordlist);
                        }
                    }
                }
            }

        };

        filter = new IntentFilter();
        filter.addAction("com.google.android.c2dm.intent.RECEIVE");
        filter.addAction("com.google.android.c2dm.intent.REGISTRATION");
    }

    private void sendMessage(final String id, final String myName, final String wordlist, final String myId) {


        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                List<String> regIds = new ArrayList<String>();
                String reg_device = id;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.Id", myId);
                msgParams.put("data.teammateName", myName);
                msgParams.put("data.word", wordlist);
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds, context);
                return "Sent connecting request.";
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }

    public void getWordList() {
        myDatabase = new MyDatabase(this);
        db = myDatabase.openDatabase();
        wordList = getWords(db);
        for (int i = 0; i < 9; i++) {
            wordList[i] = new String(distributeWords(wordList[i]));
            if (!wordlist.isEmpty())
                wordlist = wordlist + "," + wordList[i];
            else
                wordlist = wordlist + wordList[i];
        }

    }

    public String[] getWords(SQLiteDatabase db) {
        String[] words = new String[9];
        String sqlQuery = "select locale from android_metadata where length(locale) =9 order by random() limit 9;";
        Cursor cursor = db.rawQuery(sqlQuery, null);
        int i = 0;
        while (cursor.moveToNext()) {
            words[i] = cursor.getString(0);
            i++;
        }
        return words;
    }


    public char[] distributeWords(String words) {
        Random rnd = new Random();
        int chooseOrder = rnd.nextInt(9);
        char[] newWords = new char[9];
        int orderOfLetter[] = new int[9];
        switch (chooseOrder) {
            case (0):
                orderOfLetter = new int[]{0, 4, 2, 5, 8, 7, 6, 3, 1};
                break;
            case (1):
                orderOfLetter = new int[]{1, 2, 4, 0, 3, 6, 7, 5, 8};
                break;
            case (2):
                orderOfLetter = new int[]{2, 5, 8, 4, 0, 1, 3, 6, 7};
                break;
            case (3):
                orderOfLetter = new int[]{3, 4, 0, 1, 2, 5, 8, 7, 6};
                break;
            case (4):
                orderOfLetter = new int[]{4, 8, 5, 7, 6, 3, 0, 1, 2};
                break;
            case (5):
                orderOfLetter = new int[]{5, 4, 2, 1, 0, 3, 6, 7, 8};
                break;
            case (6):
                orderOfLetter = new int[]{6, 7, 8, 5, 2, 4, 1, 3, 0};
                break;
            case (7):
                orderOfLetter = new int[]{7, 4, 8, 5, 2, 1, 0, 3, 6};
                break;
            case (8):
                orderOfLetter = new int[]{8, 4, 0, 1, 2, 5, 7, 6, 3};
                break;
        }
        int i = 0;
        while (i < 9) {
            int id = orderOfLetter[i];
            newWords[id] = (words.charAt(i));
            i++;
        }
        return newWords;
    }
    public void activeNoteReciever(){
        ComponentName componentName = new ComponentName(context,TwoPlayerReceiver.class);
        context.getPackageManager().setComponentEnabledSetting
                (componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,PackageManager.DONT_KILL_APP);
    }
    public void inactiveNoteReciever(){
        ComponentName componentName = new ComponentName(context,TwoPlayerReceiver.class);
        context.getPackageManager().setComponentEnabledSetting
                (componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP);
    }
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        inactiveNoteReciever();
        registerReceiver(receiver, filter);
    }
}
