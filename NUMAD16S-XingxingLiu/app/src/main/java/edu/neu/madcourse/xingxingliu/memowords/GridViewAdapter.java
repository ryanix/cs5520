package edu.neu.madcourse.xingxingliu.memowords;

/**
 * Created by cao on 4/10/16.
 */

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

public class GridViewAdapter extends ArrayAdapter<Record> {
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<Record> mGridData = new ArrayList<>();

    public GridViewAdapter(Context mContext, int layoutResourceId){
        super(mContext, layoutResourceId);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
    }
    public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<Record> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }

    /**
     * Updates grid data and refresh grid items.
     * @param mGridData
     */
    public void setGridData(ArrayList<Record> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public void add(Record object) {
        super.add(object);
        mGridData.add(object);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.templateTextView = (TextView) row.findViewById(R.id.grid_item_template_text);
            holder.playImageView = (ImageView) row.findViewById(R.id.grid_item_play_image);
            holder.backPlayImageView = (ImageView) row.findViewById(R.id.grid_item_record_button);
            holder.backTextView = (TextView) row.findViewById(R.id.grid_item_record_text);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        Record item = mGridData.get(position);
        holder.templateTextView.setText(Html.fromHtml(item.getTemplate()));

        // Set click listeners
        holder.backPlayImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return row;
    }

    static class ViewHolder {
        TextView templateTextView;
        ImageView playImageView;
        ImageView backPlayImageView;
        TextView backTextView;
    }
}