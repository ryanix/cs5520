package edu.neu.madcourse.xingxingliu.memowords;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by cao on 4/10/16.
 */
public class MemoWordsRecordsFragment extends Fragment {

    private GridView mGridView;
    private GridViewAdapter mGridAdapter;
    private ArrayList<Record> mGridData;
    private String time;
    private String template;
    MediaPlayer mediaPlayer = new MediaPlayer();
    private DatePicker datePicker;
    private Button searchRecords;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.memo_records_fragment, container, false);
        mGridView = (GridView) rootView.findViewById(R.id.memo_records_gridview);
        datePicker = (DatePicker) rootView.findViewById(R.id.record_datePicker);
        searchRecords = (Button) rootView.findViewById(R.id.record_search);
        /* get current time*/
        DateFormat df = new SimpleDateFormat("MM/dd/yy");
        time = df.format(new Date());

        /* get today's record*/
        List<Record> recordList= new VoiceMemoDatabase(getActivity()).getRecordByDate(time);

        mGridData = new ArrayList<>();
        /* put records into current grid view*/
        for(int i = 0;i<recordList.size();i++){
            Record record = recordList.get(i);
            mGridData.add(record);
        }
        mGridAdapter = new GridViewAdapter(getContext(), R.layout.memo_grid_item, mGridData);
        mGridView.setAdapter(mGridAdapter);

        /* set onClicklistener for datepicker*/
        searchRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String time1 = time;
                Date now = new Date(datePicker.getYear(),datePicker.getMonth(),datePicker.getDayOfMonth());
                DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");
                time = dateFormat.format(now);

                /* get today's record*/
                List<Record> recordList= new VoiceMemoDatabase(getActivity()).getRecordByDate(time);

                mGridData = new ArrayList<>();
        /* put records into current grid view*/
                for(int i = 0;i<recordList.size();i++){
                    Record record = recordList.get(i);
                    mGridData.add(record);
                }
                mGridAdapter.clear();
                mGridAdapter = new GridViewAdapter(getContext(), R.layout.memo_grid_item, mGridData);
                mGridView.setAdapter(mGridAdapter);
            }
        });
        return rootView;
    }



    // Inner class for a custom GridViewAdapter
    private class GridViewAdapter extends ArrayAdapter<Record> {
        private Context mContext;
        private int layoutResourceId;
        private ArrayList<Record> mGridData = new ArrayList<>();

        public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<Record> mGridData) {
            super(mContext, layoutResourceId, mGridData);
            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.mGridData = mGridData;
        }

        /**
         * Updates grid data and refresh grid items.
         *
         * @param mGridData
         */
        public void setGridData(ArrayList<Record> mGridData) {
            this.mGridData = mGridData;
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View row = convertView;
            final ViewHolder holder;

            if (row == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                holder.frontView = row.findViewById(R.id.grid_item_template_layout);
                holder.backView = row.findViewById(R.id.grid_item_record_layout);
                holder.templateTextView = (TextView) row.findViewById(R.id.grid_item_template_text);
                holder.playImageView = (ImageView) row.findViewById(R.id.grid_item_play_image);
                holder.backPlayImageView = (ImageView) row.findViewById(R.id.grid_item_record_button);
                holder.backTextView = (TextView) row.findViewById(R.id.grid_item_record_text);
                holder.previewTextView = (ImageView) row.findViewById(R.id.grid_item_preview_button);
                holder.emptyView = row.findViewById(R.id.grid_item_empty_view);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            final Record item = mGridData.get(position);
            holder.templateTextView.setText(Html.fromHtml(item.getTemplate()));
            holder.backTextView.setText("Click icon to begin");

            // Set click listeners
            holder.emptyView.setOnClickListener(null);

            holder.playImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Play the audio
                    template = item.template;
                    List<Record> records = new VoiceMemoDatabase(getActivity()).getSimilarRecord(template, time);
                    if (!records.isEmpty()) {
                        byte[] audioByte = records.get(0).record;
                        playMp3(audioByte);
                    }
                }
            });

            holder.previewTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    template = item.template;
                    // set popup menu
                    final PopupMenu popupMenu = new PopupMenu(getContext(), v);
                    popupMenu.getMenuInflater().inflate(R.menu.record_menu_item, popupMenu.getMenu());
                    popupMenu.show();
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.record_action_preview:
                                    retrieveAudioText(position);
                                    return true;
                                case R.id.record_action_delete:
                                    new VoiceMemoDatabase(getActivity()).deleteRecord(time,template);
                                    mGridData.remove(position);
                                    mGridAdapter = new GridViewAdapter(getContext(), R.layout.memo_grid_item, mGridData);
                                    mGridView.setAdapter(mGridAdapter);
                                    return true;
                                case R.id.record_action_share:
                                    return true;
                            }
                            return false;
                        }
                    });

                }
            });
            return row;
        }

        // play audio from byte array
        private void playMp3(byte[] mp3SoundByteArray) {
            try {
                // create temp file that will hold byte array
                File tempMp3 = File.createTempFile("temAudio", "mp3", getActivity().getCacheDir());
                tempMp3.deleteOnExit();
                FileOutputStream fos = new FileOutputStream(tempMp3);
                fos.write(mp3SoundByteArray);
                fos.close();

                FileInputStream fis = new FileInputStream(tempMp3);
                mediaPlayer.reset();
                mediaPlayer.setDataSource(fis.getFD());

                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException ex) {
                String s = ex.toString();
                ex.printStackTrace();
            }
        }

        private void retrieveAudioText(int position){
            //retrieve record from database with same template and time
            DateFormat df = new SimpleDateFormat("MM/dd/yy");
            time = df.format(new Date());
            template = mGridData.get(position).template;
            List<Record> records = new VoiceMemoDatabase(getActivity()).getSimilarRecord(template, time);
            String databaseResult="";
            if(!records.isEmpty()){
                databaseResult = databaseResult + records.get(0).preview;
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setMessage(databaseResult);
                dialog.setTitle("Textual Preview");
                dialog.setPositiveButton("OK",null);
                dialog.show();
            }
        }


        private final class ViewHolder {
            TextView templateTextView;
            ImageView playImageView;
            ImageView backPlayImageView;
            TextView backTextView;
            ImageView previewTextView;
            View frontView;
            View backView;
            View emptyView;
        }

    }


}


