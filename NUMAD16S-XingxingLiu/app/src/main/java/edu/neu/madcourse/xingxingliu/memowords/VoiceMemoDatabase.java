package edu.neu.madcourse.xingxingliu.memowords;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ryanix on 4/10/16.
 */

/*
 Database used to store local templates
 */
public class VoiceMemoDatabase extends SQLiteOpenHelper {

    public static final String DATABASENAME = "VoiceMemoDatabse";
    public static final String TABLE_RECORD = "VoiceMemoTable";
    public static final String TABLE_TEMPLATE = "VoiceMemoTemplate";

    public static final String RECORD_ID = "id";
    public static final String RECORD_TIME = "recordTime";
    public static final String RECORD_LOCATION = "recordLocation";
    public static final String RECORD_TEMPLATE = "template";
    public static final String RECORD_AUDIO = "record";
    public static final String RECORD_PREVIEW = "preview";

    public static final String TEMPLATE_ID = "id";
    public static final String TEMPLATE_NAME = "template";

    public static final String CREATE_TABLE_RECORDS =
            "create table " + TABLE_RECORD +
                    "(" + RECORD_ID + " integer primary key autoincrement," +
                    RECORD_TIME + " text, " +
                    RECORD_LOCATION + " text, " +
                    RECORD_TEMPLATE + " text, " +
                    RECORD_AUDIO + " Blob not null, " +
                    RECORD_PREVIEW + " text, " +
                    "foreign key (" + RECORD_TEMPLATE + ") references  " + TABLE_TEMPLATE + "(" + TEMPLATE_ID + "));";

    public static final String CREATE_TABLE_TEMPLATE =
            "create table " + TABLE_TEMPLATE +
                    "(" + TEMPLATE_ID + " integer primary key autoincrement, " +
                    TEMPLATE_NAME + " text not null);";

    /*
    constructor of voiceMemo database
     */
    public VoiceMemoDatabase(Context context) {
        super(context, DATABASENAME, null, 1);
    }

    /*
    onCreate method called when the database is created the first time
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TEMPLATE);
        db.execSQL(CREATE_TABLE_RECORDS);
        initTemplateTable(db);
        //TODO initialize template table
    }

    //initialize template table
    public void initTemplateTable(SQLiteDatabase db) {
        String[] templates = {
                "Did I exercise today?",
                "Am I in good health today?",
                "Am I in high spirits today?",
                "What did I eat today?",
                "How was my work balance today?",
                "What funny things happened to my pet today?",
                "Did I take care of my plants today",
                "What book did I read today",
                "What movie did I watch today?",
                "What matters of interest happened today?",
                "What did I do for my family today?",
                "Did I care for my parent today?"
        };
        for (int i = 0; i < templates.length; i++) {
            ContentValues values = new ContentValues();
            values.put(TEMPLATE_NAME, templates[i]);
            db.insert(TABLE_TEMPLATE, null, values);
        }
    }

    /*
    onUpgrade method, called when the database upgrades to a new version
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /*
    onOpen method, called when the template database is called
     */
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    /*
    add a new record into audio record table
     */
    public void addRecord(Record record) {
        String time = record.recordTime;
        String template = record.template;
        List<Record> checkRecord = getSimilarRecord(template, time);
        ContentValues values = new ContentValues();
        values.put(RECORD_TIME, record.recordTime);
        values.put(RECORD_LOCATION, record.recordLocation);
        values.put(RECORD_TEMPLATE, record.template);
        values.put(RECORD_AUDIO, record.record);
        values.put(RECORD_PREVIEW, record.preview);

        SQLiteDatabase db = this.getWritableDatabase();
        if (checkRecord.isEmpty()) {

            db.insert(TABLE_RECORD, null, values);
        }
        else{
            String[] args = {template,time};
            db.update(TABLE_RECORD, values, "template = ? and recordTime = ?", args);
        }

        db.close();
    }
    /* delete a record*/
    public void deleteRecord(String time,String template){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] argus= {time,template};
        db.delete(TABLE_RECORD,"recordTime = ? and template = ?",argus);
        db.close();
    }

    /* delete a template */
    public void deleteTemplate(String template){
        SQLiteDatabase db = this.getWritableDatabase();
        String[] argus = {template};
        db.delete(TABLE_TEMPLATE,"template = ?",argus);
        db.close();
    }
    /*
    add a new template to local template table
     */
    public void addTemplate(String template) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TEMPLATE_NAME, template);
        db.insert(TABLE_TEMPLATE, null, values);
        db.close();
    }

    /*
    check for duplicate template
     */
    public Boolean checkDuplicateTemplate(String template) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select " + TEMPLATE_NAME + " from " + TABLE_TEMPLATE + " where template = ?;";
        String[] args = {template};
        Cursor cursor = db.rawQuery(selectQuery, args);
        return(cursor.moveToFirst());
    }

    /*
    fetch records from the database based on template and time
     */
    public List<Record> getSimilarRecord(String template, String time) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select * " +
                "from VoiceMemoTable " +
                "where template = ? and recordTime = ?;";
        String[] argus = {template, time};
        Cursor cursor = db.rawQuery(query, argus);
        List<Record> records = new ArrayList<>();
        while (cursor.moveToNext()) {
            Record record = new Record(
                    cursor.getString(cursor.getColumnIndex(RECORD_TIME)),
                    cursor.getString(cursor.getColumnIndex(RECORD_LOCATION)),
                    cursor.getString(cursor.getColumnIndex(TEMPLATE_NAME)),
                    cursor.getBlob(cursor.getColumnIndex(RECORD_AUDIO)),
                    cursor.getString(cursor.getColumnIndex(RECORD_PREVIEW))
            );
            records.add(record);
        }
        db.close();
        return records;
    }
    /*
    fetch records from the database based on time frame
     */

    public List<Record> getRecordByDate(String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select * " +
                "from VoiceMemoTable " +
                "where recordTime = ?;";
        String[] argus = {date};
        Cursor cursor = db.rawQuery(query, argus);
        List<Record> records = new ArrayList<Record>();
        while (cursor.moveToNext()) {
            Record record = new Record(
                    cursor.getString(cursor.getColumnIndex(RECORD_TIME)),
                    cursor.getString(cursor.getColumnIndex(RECORD_LOCATION)),
                    cursor.getString(cursor.getColumnIndex(TEMPLATE_NAME)),
                    cursor.getBlob(cursor.getColumnIndex(RECORD_AUDIO)),
                    cursor.getString(cursor.getColumnIndex(RECORD_PREVIEW))
            );
            records.add(record);
        }
        db.close();
        return records;
    }

    /*
    fetch all templates from the database
     */

    public List<String> getTemplates() {
        List<String> templates = new ArrayList<String>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select " + TEMPLATE_NAME + " from " + TABLE_TEMPLATE + ";";
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            String template = cursor.getString(0);
            templates.add(template);
        }
        db.close();
        return templates;
    }

    // randomly pick 6 templates from the database
    public List<String> pickTemplates() {
        List<String> templates = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "select " + TEMPLATE_NAME + " from " + TABLE_TEMPLATE + " order by random() limit 6;";
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            String template = cursor.getString(0);
            templates.add(template);
        }
        db.close();
        return templates;
    }
}
