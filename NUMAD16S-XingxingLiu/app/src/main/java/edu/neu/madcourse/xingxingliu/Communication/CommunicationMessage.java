package edu.neu.madcourse.xingxingliu.Communication;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by Ryanix on 3/16/16.
 */
public class CommunicationMessage extends AppCompatActivity implements OnClickListener {
    private static String vsPlayerName;
    private static String vsPlayerId;
    private TextView Name;
    private TextView Message;
    private EditText Input;
    private Button Send;
    private Context context;
    private Button Acknowledgement;
    private AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        context=this;
        setContentView(R.layout.communication_message);
        vsPlayerName = getIntent().getExtras().getString("VsPlayerName");
        vsPlayerId = getIntent().getExtras().getString("VsPlayerId");
        Name = (TextView) findViewById(R.id.receiver_name);
        Message = (TextView) findViewById(R.id.text_message_recieved);
        Input = (EditText) findViewById(R.id.test_message);
        Send = (Button) findViewById(R.id.test_send);
        Acknowledgement = (Button) findViewById(R.id.communication_acknowledgement);
        Name.setText(vsPlayerName);

    }

    @Override
    public void onClick(View view) {
        if(view == Input){
            Input.setText("");
        }
        else if (view == Acknowledgement){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Communication Ackonwledgement");
            builder.setMessage(R.string.acknowledgements);
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.ok_label,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
            mDialog = builder.show();
        }
        else if(view == Send){
            String msg = Input.getText().toString();
            sendMessage(msg);
        }

    }

    private void sendMessage(final String message) {

        if (message.isEmpty()) {
            Toast.makeText(this, "Empty Message", Toast.LENGTH_LONG).show();
            return;
        }

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                List<String> regIds = new ArrayList<String>();
                String reg_device = vsPlayerId;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.message", message);
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds,CommunicationMessage.this);
                return "Message Sent - " + message;
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }
    private static void setSendMessageValues(String msg) {
        CommunicationConstants.alertText = "Message Notification";
        CommunicationConstants.titleText = "Sending Message";
        CommunicationConstants.contentText = msg;
    }
}
