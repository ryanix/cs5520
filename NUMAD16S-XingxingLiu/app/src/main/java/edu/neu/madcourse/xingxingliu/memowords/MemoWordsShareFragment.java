package edu.neu.madcourse.xingxingliu.memowords;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by cao on 4/10/16.
 */
public class MemoWordsShareFragment extends Fragment {
    public static final String FROM_SHARE_FRAGMENT = "from_share_fragment";
    private String TAG = "MemoWordsShareFragment";
    private GridView mGridView;
    private ArrayList<Record> mGridData;
    private GridViewAdapter mGridAdapter;
    private static String FIREBASE_URL = "https://memoword.firebaseio.com";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.memo_share_fragment, container, false);
        // Initialize views
        mGridView = (GridView) rootView.findViewById(R.id.memo_share_gridview);
        // Initialize grid data
        mGridData = new ArrayList<>();

        fillGridFromFirebase();

//        mGridAdapter = new GridViewAdapter(getContext(), R.layout.memo_grid_item, mGridData);
//        mGridView.setAdapter(mGridAdapter);

        return rootView;
    }

    private void fillGridFromFirebase(){
        if (isNetworkAvailable()){
            final Firebase myFirebaseRef = new Firebase(FIREBASE_URL);
            Log.d(TAG, "Before data listener");
            myFirebaseRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d(TAG, "Before for loop");
                    mGridData.clear();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Record record = new Record();
                        record.setTemplate((String)snapshot.getValue());
                        Log.d(TAG, mGridData.toString());
                        if (snapshot.getValue() != null){
                            Log.d(TAG, "Record: " + record.getTemplate());
                            //mGridAdapter.add(record);
                            mGridData.add(record);
                        }
                    }
                    // cannot write this outside the method
                    mGridAdapter = new GridViewAdapter(getContext(), R.layout.memo_grid_item, mGridData);
                    mGridView.setAdapter(mGridAdapter);
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

        } else {
//            Toast.makeText(getContext(), "Internet is not available! You can open the Google voice typing" +
//                    "in Language & input ", Toast.LENGTH_SHORT).show();
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Internet is not available!");
            alert.setMessage("You can open the Google voice typing" +
                    "in Language & input option, and all your words will be translated into text without key words");
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alert.show();
        }
    }

    // Check if the user can get data from Firebase
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class GridViewAdapter extends ArrayAdapter<Record> {
        private Context mContext;
        private int layoutResourceId;
        private ArrayList<Record> mGridData = new ArrayList<>();

        public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<Record> mGridData) {
            super(mContext, layoutResourceId, mGridData);
            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.mGridData = mGridData;
        }

        /**
         * Updates grid data and refresh grid items.
         *
         * @param mGridData
         */
        public void setGridData(ArrayList<Record> mGridData) {
            this.mGridData = mGridData;
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View row = convertView;
            final ViewHolder holder;

            if (row == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                Log.d(TAG, row.toString());
                holder.frontView = row.findViewById(R.id.grid_item_template_layout);
                holder.templateTextView = (TextView) row.findViewById(R.id.grid_item_template_text);
                holder.playImageView = (ImageView) row.findViewById(R.id.grid_item_play_image);
                holder.previewImageView = (ImageView) row.findViewById(R.id.grid_item_preview_button);
                holder.emptyView = row.findViewById(R.id.grid_item_empty_view);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT);
                row.setLayoutParams(new GridView.LayoutParams(params));

                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            final Record item = mGridData.get(position);
            holder.templateTextView.setText(Html.fromHtml(item.getTemplate()));

            // Set click listeners
            holder.emptyView.setOnClickListener(null);
            holder.playImageView.setVisibility(View.GONE);
            holder.previewImageView.setImageResource(R.drawable.ic_file_download_black_48dp);
            holder.previewImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Download button in share fragment");
                    // send broadcast to entry fragment
                    String entryTitle = holder.templateTextView.getText().toString();
                    Intent intent = new Intent(FROM_SHARE_FRAGMENT);
                    intent.putExtra("DOWNLOAD", entryTitle);
                    getActivity().sendBroadcast(intent);
                }
            });

            return row;
        }

        private final class ViewHolder {
            TextView templateTextView;
            ImageView playImageView;
            ImageView previewImageView;
            View frontView;
            View emptyView;
        }

    }
}
