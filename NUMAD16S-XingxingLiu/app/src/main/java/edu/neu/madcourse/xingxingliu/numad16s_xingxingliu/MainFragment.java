package edu.neu.madcourse.xingxingliu.numad16s_xingxingliu;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import edu.neu.madcourse.xingxingliu.Communication.CommunicationNamingActivity;
import edu.neu.madcourse.xingxingliu.Scraggle.WordGameActivity;
import edu.neu.madcourse.xingxingliu.TwoPlayer.TwoPlayerCommunicate;
import edu.neu.madcourse.xingxingliu.memowords.MemoWordsMainActivity;


public class MainFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.app_fragment_main, container, false);
        // Handle buttons here...
        View appAboutButton = rootView.findViewById(R.id.app_about_button);
        View appGenerateErrorButton = rootView.findViewById(R.id.app_generate_error_button);
        View appTicTacToeButton = rootView.findViewById(R.id.app_tictactoe_button);
        View appQuitButton = rootView.findViewById(R.id.app_quit_button);
        View appWordGame = rootView.findViewById(R.id.app_word_game);
        View appDictionary = rootView.findViewById(R.id.app_dictionary_button);
        View twoPlayer = rootView.findViewById(R.id.two_player);
        View appCommunication = rootView.findViewById(R.id.communication);
        View memoWords = rootView.findViewById(R.id.memo_words);
        View finalProject = rootView.findViewById(R.id.app_final_project);

        appCommunication.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(checkPlayServices()) {
                    Intent intent = new Intent(getActivity(), CommunicationNamingActivity.class);
                    getActivity().startActivity(intent);
                }
                else
                {
                    String msg="Device not supported.";
                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                }
            }
        });

        appAboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent intent = new Intent(getActivity(), AboutActivity.class);
                getActivity().startActivity(intent);
            }
        });

        appTicTacToeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(getActivity(), org.example.tictactoe.MainActivity.class);
                getActivity().startActivity(intent);
            }
        });

        appGenerateErrorButton.setOnClickListener(new View.OnClickListener() {
            @Override
                    public void onClick(View view){
                throw new RuntimeException("Test runtime Exeption");

            }
        });

        appDictionary.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(getActivity(),DictionaryActivity.class);
                getActivity().startActivity(intent);
            }
        });

        appWordGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent intent = new Intent(getActivity(),WordGameActivity.class);
                getActivity().startActivity(intent);
            }
        });

        twoPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TwoPlayerCommunicate.class);
                getActivity().startActivity(intent);
            }
        });
        appQuitButton.setOnClickListener(new View.OnClickListener() {
            @Override
        public void onClick(View view){
                    System.exit(0);
            }
        });

        memoWords.setOnClickListener(new View.OnClickListener(){
            @Override
        public void onClick(View v){
                Intent intent = new Intent(getActivity(), MemoWordsMainActivity.class);
                getActivity().startActivity(intent);
            }
        });

        finalProject.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(getActivity(), MemoWordsMainActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return rootView;
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(),
                        9000).show();
            }
            return false;
        }
        return true;
    }
    @Override
    public void onPause() {
        super.onPause();
    }
}
