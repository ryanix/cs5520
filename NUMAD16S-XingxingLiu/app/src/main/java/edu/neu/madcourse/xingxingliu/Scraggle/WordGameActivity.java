package edu.neu.madcourse.xingxingliu.Scraggle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

public class WordGameActivity extends AppCompatActivity {
    public static final String KEY_WORDRESTORE = "key_wordRestore";
    public static final String PREF_WORDRESTROE = "pref_wordRestore";
    private MediaPlayer mMediaPlayer;
    private Handler mHandler = new Handler();
    private WordGameFragment wordGameFragment;
    private long timer=180000;
    private CountDownTimer countDown;
    private AlertDialog mDialog;


    public void musicControl(){
        if(mMediaPlayer.isPlaying()){
            mMediaPlayer.pause();
        }
        else{
            mMediaPlayer.start();
        }

    }
    public void setTimer() {
        final TextView countDownText = (TextView) findViewById(R.id.count_down);

            countDown =new CountDownTimer(timer, 1000) {
                String display="";
                public void onTick(long millisUntilFinished) {
                timer=millisUntilFinished;
                    long less = millisUntilFinished%90000;
                    if(millisUntilFinished<=90000){
                        if(less>=10000){
                        display = "Phase 2 Time remained: " + less/60000 + ":" + less%60000/1000;}
                        else{
                            display="In "+ less%60000/1000+" seconds, Game Over.";
                        }
                        wordGameFragment.setVisibility();
                        wordGameFragment.setPhase(2);
                    }
                    else {
                        if(less>=10000){
                            display = "Phase 1 Time remained: " + less/60000 + ":" + less%60000/1000;}
                        else{
                            display="In " + less%60000/1000+" seconds, Entering Phase 2.";
                        }
                    }

                    countDownText.setText(display);
            }

            public void onFinish() {

                    String score = wordGameFragment.computeScores()+"";
                    AlertDialog.Builder builder = new AlertDialog.Builder(WordGameActivity.this);
                    builder.setTitle(R.string.final_score);
                    builder.setMessage("Your final score is: "+ score);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.ok_label,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface,int i){}
                            });
                    mDialog=builder.show();
                }

        }.start();
    }
    public void cancelTimer(){
        countDown.cancel();

    }

    public void setBoardVisibility(int i){
        wordGameFragment.setBoardVisibility(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.word_game);
        setContentView(R.layout.word_game);
        wordGameFragment = (WordGameFragment)
                getFragmentManager().findFragmentById(R.id.fragment_word_game);
        boolean restore = getIntent().getBooleanExtra(KEY_WORDRESTORE, false);
        if (restore) {
            String gameData = getPreferences(MODE_PRIVATE)
                    .getString(PREF_WORDRESTROE, null);
            if (gameData != null) {
                wordGameFragment.putState(gameData);
            }
        }
        wordGameFragment.setViewText();
    }
    @Override
    protected void onResume() {
        super.onResume();
        setTimer();
        mMediaPlayer=new MediaPlayer().create(this,R.raw.battle);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mDialog!=null)
            mDialog.dismiss();
        mHandler.removeCallbacks(null);
        cancelTimer();
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
        String gameData = wordGameFragment.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(PREF_WORDRESTROE, gameData)
                .commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
