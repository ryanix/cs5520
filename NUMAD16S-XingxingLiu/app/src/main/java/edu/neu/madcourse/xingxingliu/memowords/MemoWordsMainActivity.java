package edu.neu.madcourse.xingxingliu.memowords;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by cao on 4/10/16.
 */
public class MemoWordsMainActivity extends AppCompatActivity {
    ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.memo_main_activity);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.memo_toolbar);
        //toolbar.setTitle("MemoWords");
        //setSupportActionBar(toolbar);

        // Add fragments into the adapter
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MemoWordsEntryFragment(), "TODAY");
        adapter.addFragment(new MemoWordsRecordsFragment(), "RECENT");
        adapter.addFragment(new MemoWordsShareFragment(), "SHARE");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

        // Initialize tab view
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
