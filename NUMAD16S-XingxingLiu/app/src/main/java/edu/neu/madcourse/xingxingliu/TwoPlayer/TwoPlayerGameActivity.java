package edu.neu.madcourse.xingxingliu.TwoPlayer;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.neu.madcourse.xingxingliu.Communication.GcmBroadcastReceiver;
import edu.neu.madcourse.xingxingliu.Communication.GcmNotification;
import edu.neu.madcourse.xingxingliu.Communication.Player;
import edu.neu.madcourse.xingxingliu.Communication.RemoteClient;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;


public class TwoPlayerGameActivity extends AppCompatActivity implements ShakeEventManager.ShakeListener {

    private Context context;
    private AlertDialog mDialog;
    private String[] wordList = new String[9];

    private String teammateId;
    private String teammateName;
    private String wordlist;
    private String myId;
    private String myName;
    private int myScore;
    private int vsScore;
    private TwoPlayerGameFragment mFragment;

    private TextView validWord;
    public static final String RESUME_GAME = "pref_game_resume";
    public static final String GAME_PROCESS = "pref_game_content";
    private MediaPlayer mMediaPlayer;
    private Handler mHandler = new Handler();
    private long timer = 180000;
    private CountDownTimer countDown;
    private final int Quit = 100;
    private WakefulBroadcastReceiver receiver;
    private IntentFilter filter;

    private RemoteClient remoteClient;
    private ShakeEventManager shakeListener;

    @Override
    protected void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        context = this;
        setContentView(R.layout.online_word_game);
        validWord = (TextView) findViewById(R.id.online_valid_wordlist);
        teammateId = getIntent().getExtras().getString("vsId");
        teammateName = getIntent().getExtras().getString("vsName");
        wordlist = getIntent().getExtras().getString("word");
        myId = getIntent().getExtras().getString("myId");
        myName = getIntent().getExtras().getString("myName");
        myScore = getIntent().getExtras().getInt("myScore");
        vsScore = getIntent().getExtras().getInt("vsScore");
        wordList = wordlist.split(",");
        getSupportActionBar().setTitle("Playing With " + teammateName);
        mFragment = (TwoPlayerGameFragment)
                getFragmentManager().findFragmentById(R.id.online_fragment_word_game);
        SharedPreferences pref = getSharedPreferences(TwoPlayerGameActivity.class.getSimpleName(), MODE_PRIVATE);
        Boolean resume = pref.getBoolean(RESUME_GAME, false);
        String state = pref.getString(GAME_PROCESS, "");
        if (resume) {
            mFragment.putState(state);
        } else
            mFragment.setViewText(wordList);
        setReceiver();
        remoteClient = new RemoteClient(context);
        getPreferences(MODE_PRIVATE).edit().putBoolean(RESUME_GAME, true);
        shakeListener = new ShakeEventManager();
        shakeListener.setListener(this);
        shakeListener.init(this);
    }

    private void setReceiver() {
        receiver = new WakefulBroadcastReceiver() {
            @Override
            public void onReceive(final Context context, Intent intent) {
                Bundle extra = intent.getExtras();
                if (!extra.isEmpty()) {
                    final String large = extra.getString("large");
                    final String small = extra.getString("small");
                    if (large != null && small != null) {
                        int l = Integer.valueOf(large);
                        int s = Integer.valueOf(small);
                        if (l == Quit) {
                            Toast.makeText(context, "Your teammate has quit.", Toast.LENGTH_LONG).show();
                        } else {
                            mFragment.updateAvailable(l, s);
                            mFragment.updateWordTile();
                        }
                    }
                }
            }

        };

        filter = new IntentFilter();
        filter.addAction("com.google.android.c2dm.intent.RECEIVE");
    }

    public void sendMessage(final int large, final int small) {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                List<String> regIds = new ArrayList<String>();
                String reg_device = teammateId;
                Map<String, String> msgParams;
                msgParams = new HashMap<>();
                msgParams.put("data.large", large + "");
                msgParams.put("data.small", small + "");
                msgParams.put("data.id", myId);
                msgParams.put("data.name", myName);
                msgParams.put("data.vsId", teammateId);
                msgParams.put("data.vsName", teammateName);
                msgParams.put("data.myScore", myScore + "");
                msgParams.put("data.vsScore", vsScore + "");
                GcmNotification gcmNotification = new GcmNotification();
                regIds.clear();
                regIds.add(reg_device);
                gcmNotification.sendNotification(msgParams, regIds, TwoPlayerGameActivity.this);
                return "Wait for your teammate's move ";
            }

            @Override
            protected void onPostExecute(String msg) {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
            }
        }.execute(null, null, null);
    }

    public void musicControl() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
        } else {
            mMediaPlayer.start();
        }

    }

    public void setTimer() {
        final TextView countDownText = (TextView) findViewById(R.id.online_count_down);

        countDown = new CountDownTimer(timer, 1000) {
            String display = "";

            public void onTick(long millisUntilFinished) {
                timer = millisUntilFinished;
                if (millisUntilFinished > 30000) {
                    display = " Time remained: " + millisUntilFinished / 60000 + ":" + millisUntilFinished % 60000 / 1000;
                } else {
                    display = "In " + millisUntilFinished % 60000 / 1000 + " seconds, Game Over.";
                }

                countDownText.setText(display);
            }

            public void onFinish() {
                myScore = myScore + mFragment.computeScores();
                AlertDialog.Builder builder = new AlertDialog.Builder(TwoPlayerGameActivity.this);
                builder.setTitle(R.string.final_score);
                builder.setMessage("Your final score is: " + myScore);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                updateScore();
                                getPreferences(MODE_PRIVATE).edit().putBoolean(RESUME_GAME, false);
                                Intent intent = new Intent(context, TwoPlayerCommunicate.class);
                                startActivity(intent);
                            }
                        });
                mDialog = builder.show();
                unregisterReceiver(receiver);
            }

        }.start();
    }

    public void showValidWord(String s) {
        String current = validWord.getText().toString();
        if (current.isEmpty())
            current = current + s;
        else
            current = current + ", " + s;
        Toast.makeText(context, "new word added!", Toast.LENGTH_LONG).show();
        validWord.setText(current);

    }

    public void updateScore() {
        Player player = new Player(myName, myId, myScore);
        remoteClient.saveValue("Player", myName, player);
    }

    public void cancelTimer() {
        countDown.cancel();

    }

    public void finishGame() {
        sendMessage(Quit,Quit);
        getPreferences(MODE_PRIVATE).edit().putBoolean(RESUME_GAME, false);
        finish();
    }

    @Override
    public void onShake() {
        mFragment.resetWordTile();
    }

    public void activeNoteReciever(){
        ComponentName componentName = new ComponentName(context,TwoPlayerReceiver.class);
        context.getPackageManager().setComponentEnabledSetting
                (componentName,PackageManager.COMPONENT_ENABLED_STATE_ENABLED,PackageManager.DONT_KILL_APP);
    }
    public void inactiveNoteReciever(){
        ComponentName componentName = new ComponentName(context,TwoPlayerReceiver.class);
        context.getPackageManager().setComponentEnabledSetting
                (componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,PackageManager.DONT_KILL_APP);
    }
    @Override
    protected void onPause() {
        super.onPause();
        activeNoteReciever();
        shakeListener.unregister();
        if (mDialog != null)
            mDialog.dismiss();
        mHandler.removeCallbacks(null);
        cancelTimer();
        mMediaPlayer.stop();
        mMediaPlayer.reset();
        mMediaPlayer.release();
        String gameData = mFragment.getState();
        getPreferences(MODE_PRIVATE).edit()
                .putString(GAME_PROCESS, gameData)
                .putBoolean(RESUME_GAME, true)
                .commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
        inactiveNoteReciever();
        shakeListener.register();
        setTimer();
        mMediaPlayer = new MediaPlayer().create(this, R.raw.battle);
        mMediaPlayer.setLooping(true);
        mMediaPlayer.start();
    }
}
