package edu.neu.madcourse.xingxingliu.numad16s_xingxingliu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.my_title);
        setContentView(R.layout.about);
    }

}
