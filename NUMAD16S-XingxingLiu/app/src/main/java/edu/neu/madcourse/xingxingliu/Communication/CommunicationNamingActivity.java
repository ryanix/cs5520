package edu.neu.madcourse.xingxingliu.Communication;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.util.ArrayList;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

public class CommunicationNamingActivity extends AppCompatActivity implements OnClickListener {

    private static final String FIREBASE_DB = "https://luminous-fire-4892.firebaseIO.com";
    private Button confirm;
    private EditText inputUsername;
    private Button showContent;
    private TextView title;
    private TableRow registration;
    private ListView playersInfo;
    PlayerAdapter adapter;
    public CommunicationNamingActivity CustomListView = null;
    public ArrayList<Player> players = new ArrayList<Player>();


    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    static final String TAG = "GCM Sample Demo";
    GoogleCloudMessaging gcm;
    Context context;
    String regid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Communication Test");
        setContentView(R.layout.communication_registration);
        showContent = (Button) findViewById(R.id.show_content);
        inputUsername = (EditText) findViewById(R.id.username_input);
        registration = (TableRow) findViewById(R.id.registration_view);
        title = (TextView) findViewById(R.id.title_for_registration);
        playersInfo = (ListView) findViewById(R.id.playerList);
        confirm = (Button) findViewById(R.id.registration_confirm);
        gcm = GoogleCloudMessaging.getInstance(this);
        context = getApplicationContext();
        CustomListView = this;
        if (getRegistrationId(context) != "") {
            title.setText("Players List");
            registration.setVisibility(View.GONE);
        }
        fillPlayerList();
    }

    @SuppressLint("NewApi")
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION,
                Integer.MIN_VALUE);

        return registrationId;
    }

    public Boolean checkUsername(String username) {
        for (Player p : players) {
            if (p.getUsername() == username)
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if (view == inputUsername) {
            inputUsername.setText("");
        } else if (view == showContent) {
            fillPlayerList();

        } else if (view == confirm) {
            String input = inputUsername.getText().toString();
            if (inputUsername == null) {
                Toast.makeText(context, "Name can not be empty", Toast.LENGTH_LONG).show();
            } else if (checkUsername(input)) {
                Toast.makeText(context, "Name already exists", Toast.LENGTH_LONG).show();
                inputUsername.setText("");
            } else {
                if (checkPlayServices()) {
                    Toast.makeText(context, "Registering~!", Toast.LENGTH_LONG).show();
                    registerInBackground(input);
                }
                inputUsername.setText(regid);
            }
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        9000).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(CommunicationNamingActivity.class.getSimpleName(), Context.MODE_PRIVATE);
    }


    private void registerInBackground(String username) {
        new AsyncTask<String, Void, String>() {
            @Override
            protected String doInBackground(String... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(CommunicationConstants.GCM_SENDER_ID);
                    Player player = new Player(params[0], regid, 0);
                    RemoteClient remoteClient = new RemoteClient(context);
                    remoteClient.saveValue("Player", params[0], player);
                    msg = "Device registered";
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage() + " Please try again";
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result == "Device registered") {
                    showContent.setVisibility(View.VISIBLE);
                    playersInfo.setVisibility(View.VISIBLE);
                    registration.setVisibility(View.GONE);
                    title.setText("Players List");
                }
                Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
            }
        }.execute(username);
    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.commit();
    }

    public void fillPlayerList() {
        Firebase.setAndroidContext(context);
        Firebase ref = new Firebase(FIREBASE_DB + "/" + "Player");
        Query queryRef = ref.orderByChild("score");
        RemoteClient remoteClient = new RemoteClient(context);
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // snapshot contains the key and value
                if (snapshot.getValue() != null) {
                    Log.d(TAG, "Data Received" + snapshot.getValue().toString());

                    // Adding the data to the ArrayList
                    for (DataSnapshot postshot : snapshot.getChildren()) {
                        Player player = postshot.getValue(Player.class);
                        if(player.getId() != regid)
                            players.add(player);
                    }
                } else {
                    Log.d(TAG, "Data Not Received");
                }
                PlayerAdapter adapter = new PlayerAdapter(CustomListView, players);
                playersInfo.setAdapter(adapter);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(TAG, firebaseError.getMessage());
                Log.e(TAG, firebaseError.getDetails());
            }
        });
        players.addAll(remoteClient.getPlayerInfo());

    }


    public void onItemClick(Integer mPostion) {
        Player player = players.get(mPostion);
        String vsPlayer = player.getUsername();
        String vsId = player.getId();
        Intent intent = new Intent(this,CommunicationMessage.class);
        intent.putExtra("VsPlayerName",vsPlayer);
        intent.putExtra("VsPlayerId",vsId);
        startActivity(intent);
    }

}
