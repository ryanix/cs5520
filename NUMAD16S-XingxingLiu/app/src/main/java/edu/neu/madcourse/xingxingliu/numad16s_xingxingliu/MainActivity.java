package edu.neu.madcourse.xingxingliu.numad16s_xingxingliu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ProgressBar;

import java.io.File;
import java.io.IOException;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

public class MainActivity extends AppCompatActivity {

    private MyDatabase myDatabase;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle(R.string.my_title);
        File dbFile = this.getDatabasePath("wordList.db");
        if(!dbFile.exists()) {

            myDatabase = new MyDatabase(this);
            new PrepareDatabase().execute();
        }
        setContentView(R.layout.app_activity_main);
    }


    public class PrepareDatabase extends AsyncTask<String,Integer,String>{

        ProgressDialog progressBar = new ProgressDialog(MainActivity.this);
        int progress = 0;

        @Override
        protected void onPreExecute() {
            progressBar.setCancelable(false);
            progressBar.setMessage("File copying ...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressBar.incrementProgressBy(0);
            progressBar.setMax(100);
            progressBar.show();
        }

        protected String doInBackground(String...strings){
            try {
                while(progress<=100) {
                    publishProgress(progress);
                    progress=progress+10;
                }
                myDatabase.createDatebase();

            } catch (IOException e) {
                e.printStackTrace();
            }

            return "";
        }
        protected void onProgressUpdate(Integer... progress) {
            progressBar.setProgress(progress[0]);
        }

        protected void onPostExecute(String result) {
            progressBar.dismiss();
            AlertDialog.Builder dialog=new AlertDialog.Builder(MainActivity.this);
            dialog.setMessage("Installation Completed!!");
            dialog.setTitle("Installing Dictionary");
            dialog.setPositiveButton("OK",null);
            dialog.show();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
