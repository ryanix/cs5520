package edu.neu.madcourse.xingxingliu.Scraggle;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.MyDatabase;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

public class WordGameFragment extends Fragment {
   static private int mLargeIds[] = {R.id.large01,R.id.large02, R.id.large03,
         R.id.large04, R.id.large05, R.id.large06, R.id.large07, R.id.large08,
         R.id.large09,};
   static private int mSmallIds[] = {R.id.small01, R.id.small02, R.id.small03,
         R.id.small04, R.id.small05, R.id.small06, R.id.small07, R.id.small08,
         R.id.small09,};
   private Handler mHandler = new Handler();
   private WordTile mEntireBoard = new WordTile(this);
   private WordTile mLargeWordTiles[] = new WordTile[9];
   private WordTile mSmallWordTiles[][] = new WordTile[9][9];
   private Set<WordTile> mAvailable = new HashSet<WordTile>();
   private Set<WordTile> mPressed = new HashSet<WordTile>();
   private int mSoundX, mSoundO, mSoundMiss, mSoundRewind;
   private SoundPool mSoundPool;
   private float mVolume = 1f;
   private MyDatabase myDatabase;
   private SQLiteDatabase db;
   private String[] words;
    private int phase = 1;
    private ArrayList<String> validWordList1;
    private ArrayList<String> validWordList2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
//      myDatabase=new MyDatabase(getActivity().getApplicationContext());
//      db=myDatabase.openDatabase();
//      words = getWords();
      setRetainInstance(true);
      initGame();
      mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
      mSoundX = mSoundPool.load(getActivity(), R.raw.sergenious_moveo, 1);
      mSoundO = mSoundPool.load(getActivity(), R.raw.sergenious_movex, 1);
      mSoundMiss = mSoundPool.load(getActivity(), R.raw.erkanozan_miss, 1);
      mSoundRewind = mSoundPool.load(getActivity(), R.raw.joanne_rewind, 1);
   }
   @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {

       View rootView =
               inflater.inflate(R.layout.word_large_board, container, false);

       initViews(rootView);

       return rootView;
   }
    private void addAllPressedTiles()
    {
        mAvailable.clear();
        for(int i=0;i<9;i++){
            if(mLargeWordTiles[i].getValidStatus()){
                for(WordTile w:mLargeWordTiles[i].getPressedChild()){
                    mAvailable.add(w);

                }
            }
        }
    }
    public String[] getWords(SQLiteDatabase db){
      String[] words = new String[9];
      String sqlQuery="select locale from android_metadata where length(locale) =9 order by random() limit 9;";
      Cursor cursor = db.rawQuery(sqlQuery,null);
      int i = 0;
      while(cursor.moveToNext()){
         words[i]=cursor.getString(0);
         i++;
      }
       return words;
   }
    public void restWordTile(WordTile w){
        w.clearWord();
        w.clearPressedChild();
        w.setValidStatus(false);
    }
    public void restartGame(){
        restWordTile(mEntireBoard);
        for(int i=0;i<9;i++){
            restWordTile(mLargeWordTiles[i]);
            for(int j=0;j<9;j++){
                restWordTile(mSmallWordTiles[i][j]);
            }
        }
        setAllAvailable();
        mPressed.clear();
        setViewText();
    }
    public char[] distributeWords(String words) {
      Random rnd = new Random();
      int chooseOrder = rnd.nextInt(9);
      char[] newWords= new char[9];
      int orderOfLetter[]=new int[9];
      switch (chooseOrder) {
         case (0):orderOfLetter=new int[]{0,4,2,5,8,7,6,3,1};break;
         case (1):orderOfLetter=new int[]{1,2,4,0,3,6,7,5,8};break;
         case (2):orderOfLetter=new int[]{2,5,8,4,0,1,3,6,7};break;
         case (3):orderOfLetter=new int[]{3,4,0,1,2,5,8,7,6};break;
         case (4):orderOfLetter=new int[]{4,8,5,7,6,3,0,1,2};break;
         case (5):orderOfLetter=new int[]{5,4,2,1,0,3,6,7,8};break;
         case (6):orderOfLetter=new int[]{6,7,8,5,2,4,1,3,0};break;
         case (7):orderOfLetter=new int[]{7,4,8,5,2,1,0,3,6};break;
         case (8):orderOfLetter=new int[]{8,4,0,1,2,5,7,6,3};break;
      }
      int i=0;
      while(i<9){
         int id=orderOfLetter[i];
         newWords[id]=(words.charAt(i));
         i++;
      }
      return newWords;
   }
    private Boolean checkWords(String words){
      Cursor cursor = db.rawQuery("select locale from android_metadata where locale =?;", new String[]{words});
      return cursor.moveToFirst();
   }
    public void setViewText(){
        myDatabase=new MyDatabase(getActivity().getApplicationContext());
        db=myDatabase.openDatabase();
        words = getWords(db);
        for (int i=0;i<9;i++){
            char[] reaarangedWords=distributeWords(words[i]);
            for (int j=0;j<9;j++){
                TextView tile =(TextView) getView().findViewById(mLargeIds[i]).findViewById(mSmallIds[j]);
                tile.setText(reaarangedWords[j]+"");
            }
        }
    }
    private void initViews(View rootView) {
      mEntireBoard.setView(rootView);
      for (int large = 0; large < 9; large++) {
         final int fLarge = large;
         View outer = rootView.findViewById(mLargeIds[large]);
         mLargeWordTiles[large].setView(outer);
         for (int small = 0; small < 9; small++) {
            final TextView inner = (TextView) outer.findViewById
                  (mSmallIds[small]);
            final int fSmall = small;
            final WordTile smallWordTile = mSmallWordTiles[large][small];
            smallWordTile.setView(inner);
            inner.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   int phaseStatus = phase;
                   if (phaseStatus == 1) {
                       if (isAvailable(smallWordTile)) {
                           mLargeWordTiles[fLarge].setWord(inner.getText().toString());
                           mLargeWordTiles[fLarge].addPressedChild(smallWordTile);
                           inner.setBackgroundColor(Color.parseColor("#D8B3B3"));
                           mSoundPool.play(mSoundX, mVolume, mVolume, 1, 0, 1f);
                           updateUnavailableTile(fLarge, fSmall);
                       } else {
                           mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
                       }
                       String words_for_look_up = mLargeWordTiles[fLarge].getWord();
                       if (words_for_look_up.length() >= 3) {
                           if (checkWords(words_for_look_up)) {
                               mLargeWordTiles[fLarge].setValidStatus(true);
                               ArrayList<WordTile> largeTile = mLargeWordTiles[fLarge].getPressedChild();
                               for (WordTile w : largeTile) {
                                   w.getView().setBackgroundColor(Color.parseColor("#4FE997"));
                               }
                           } else {
                               mLargeWordTiles[fLarge].setValidStatus(false);
                               ArrayList<WordTile> largeTile = mLargeWordTiles[fLarge].getPressedChild();
                               for (WordTile w : largeTile) {
                                   w.getView().setBackgroundColor(Color.parseColor("#D8B3B3"));
                               }
                           }
                       }
                   } else if (phaseStatus == 2) {
                       {
                           if (isAvailable(smallWordTile)) {
                               mPressed.add(smallWordTile);
                               mEntireBoard.setWord(inner.getText().toString());
                               mEntireBoard.addPressedChild(smallWordTile);
                               inner.setBackgroundColor(Color.parseColor("#D8B3B3"));
                               mSoundPool.play(mSoundX, mVolume, mVolume, 1, 0, 1f);
                               updateUnavailableTilePhaseTwo(fLarge, fSmall);
                           } else {
                               mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
                           }
                           String words_for_look_up = mEntireBoard.getWord();
                           if (words_for_look_up.length() >= 3) {
                               if (checkWords(words_for_look_up)) {
                                   mEntireBoard.setValidStatus(true);
                                   ArrayList<WordTile> largeTile = mEntireBoard.getPressedChild();
                                   for (WordTile w : largeTile) {
                                       w.getView().setBackgroundColor(Color.parseColor("#F60B0B"));
                                   }
                               } else {
                                   mEntireBoard.setValidStatus(false);
                                   ArrayList<WordTile> largeTile = mEntireBoard.getPressedChild();
                                   for (WordTile w : largeTile) {
                                       w.getView().setBackgroundColor(Color.parseColor("#D8B3B3"));
                                   }
                               }
                           }
                       }
                   }

               }
            });
         }
      }
   }
    public void setVisibility(){
        if(phase==1){
            mPressed.clear();
            addAllPressedTiles();
        for(int i=0;i<9;i++){
            if(mLargeWordTiles[i].getValidStatus()){
                for(int j=0;j<9;j++){
                    if(!mLargeWordTiles[i].getPressedChild().contains(mSmallWordTiles[i][j])){
                        mSmallWordTiles[i][j].getView().setVisibility(View.INVISIBLE);
                    }
                }
            }
            else{
                for(int j=0;j<9;j++ ){
                    mSmallWordTiles[i][j].getView().setVisibility(View.INVISIBLE);
                }
            }
        }
        }
    }
    private void updateUnavailableTilePhaseTwo(int large,int small){
        mPressed.add(mSmallWordTiles[large][small]);
        int[] neighbour=new int[]{};
        switch(large){
            case(0):neighbour=new int[]{1,4,3};
                break;
            case(1):neighbour=new int[]{0,2,3,4,5};
                break;
            case(2):neighbour=new int[]{1,5,4};
                break;
            case(3):neighbour=new int[]{0,1,4,6,7};
                break;
            case(4):neighbour=new int[]{0,1,2,3,5,6,7,8};
                break;
            case(5):neighbour=new int[]{1,2,4,7,8};
                break;
            case(6):neighbour=new int[]{3,4,7};
                break;
            case(7):neighbour=new int[]{3,4,5,6,8};
                break;
            case(8):neighbour=new int[]{4,5,7};
                break;
        }
            mAvailable.clear();
        int length=neighbour.length;
        while(length>0){
            int smallNext=neighbour[length-1];
            WordTile nextTile=mLargeWordTiles[smallNext];
            for(WordTile w: nextTile.getPressedChild()) {
                if (!mPressed.contains(w)) {
                    mAvailable.add(w);
                }
            }
            length--;
        }
    }
    private void updateUnavailableTile(int large, int small){
      mPressed.add(mSmallWordTiles[large][small]);
      int[] neighbour=new int[]{};
      switch(small){
         case(0):neighbour=new int[]{1,4,3};
            break;
         case(1):neighbour=new int[]{0,2,3,4,5};
            break;
         case(2):neighbour=new int[]{1,5,4};
            break;
         case(3):neighbour=new int[]{0,1,4,6,7};
            break;
         case(4):neighbour=new int[]{0,1,2,3,5,6,7,8};
            break;
         case(5):neighbour=new int[]{1,2,4,7,8};
            break;
         case(6):neighbour=new int[]{3,4,7};
            break;
         case(7):neighbour=new int[]{3,4,5,6,8};
            break;
         case(8):neighbour=new int[]{4,5,7};
            break;
      }
      for(int n=0;n<9;n++){
         mAvailable.remove(mSmallWordTiles[large][n]);
      }
      int length=neighbour.length;
      while(length>0){
         int smallNext=neighbour[length-1];
         WordTile nextTile=mSmallWordTiles[large][smallNext];
         if (!mPressed.contains(nextTile)){
            mAvailable.add(nextTile);
         }
         length--;
      }
   }
    public void setBoardVisibility(int visibility){
     for(int i=0;i<9;i++){
         mLargeWordTiles[i].getView().setVisibility(visibility);
     }
 }
    public void initGame() {
      Log.d("UT3", "init game");
      mEntireBoard = new WordTile(this);
      // Create all the WordTiles
      for (int large = 0; large < 9; large++) {
         mLargeWordTiles[large] = new WordTile(this);
         for (int small = 0; small < 9; small++) {
            mSmallWordTiles[large][small] = new WordTile(this);
         }
         mLargeWordTiles[large].setSubWordTiles(mSmallWordTiles[large]);
      }
      mEntireBoard.setSubWordTiles(mLargeWordTiles);

      setAllAvailable();
   }
    private void setAllAvailable() {
      for (int large = 0; large < 9; large++) {
         for (int small = 0; small < 9; small++) {
            WordTile WordTile = mSmallWordTiles[large][small];
               addAvailable(WordTile);
         }
      }
   }
    private void addAvailable(WordTile WordTile) {
        mAvailable.add(WordTile);
    }
    public void clearAvailable(){
        mPressed.clear();
        mAvailable.clear();
    }
    public void setPhase(int i){
        this.phase=i;
    }
    public boolean isAvailable(WordTile WordTile) {
        return mAvailable.contains(WordTile);
    }
    public int computeScores(){
        int score;
        int powerPhaseOne=10;
        int powerPhaseTwo=90;
        if(mEntireBoard.getValidStatus()){
            score = powerPhaseTwo * mEntireBoard.getWord().length();
        }
        else score=0;
        for(WordTile w:mLargeWordTiles){
            if(w.getValidStatus()) {
                int length = w.getWord().length();
                score = score + length * powerPhaseOne;
            }
        }
        return score;
    }
    /** Create a string containing the state of the game. */
    public String getState() {
      StringBuilder builder = new StringBuilder();
      for (int large = 0; large < 9; large++) {
         for (int small = 0; small < 9; small++) {
            if(mPressed.contains(mSmallWordTiles[large][small])){

               builder.append(large);
               builder.append(',');
               builder.append(small);
               builder.append(',');
            }
         }
      }
      builder.append("10");
      for (int large = 0; large < 9; large++) {
         for (int small = 0; small < 9; small++) {
            if(mAvailable.contains(mSmallWordTiles[large][small])){

               builder.append(large);
               builder.append(',');
               builder.append(small);
               builder.append(',');
            }
         }
      }
      return builder.toString();
   }
    /** Restore the state of the game from the given string. */
    public void putState(String gameData) {
      String[] fields = gameData.split(",");
      int index = 0;
      int length = fields.length;
      int choice = 0;
      while(index<length)
      {
         int large = Integer.parseInt(fields[index]);
         if(large == 10){
            index=index+1;
            choice=1;
            large=Integer.parseInt(fields[index]);
         }
         else {
            index=index+1;
         }
         int small = Integer.parseInt(fields[index]);
         index=index+1;
         switch(choice) {
            case(0):mPressed.add(mSmallWordTiles[large][small]);
               break;
            case(1):mAvailable.add(mSmallWordTiles[large][small]);
               break;
         }
      }
   }
}

