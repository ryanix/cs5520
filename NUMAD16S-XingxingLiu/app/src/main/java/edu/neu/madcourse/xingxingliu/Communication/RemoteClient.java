package edu.neu.madcourse.xingxingliu.Communication;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RemoteClient {

    private static final String MyPREFERENCES = "MyPrefs";
    private static final String FIREBASE_DB = "https://luminous-fire-4892.firebaseIO.com";
    private static final String TAG = "RemoteClient";
    private static boolean isDataChanged = false;
    private Context mContext;
    private HashMap<String, String> fireBaseData = new HashMap<String, String>();
    private ArrayList<Player> playerInfo = new ArrayList();

    public RemoteClient(Context mContext) {
        this.mContext = mContext;
        Firebase.setAndroidContext(mContext);

    }


    public void saveValue(String child, String key, Player value) {
        Firebase ref = new Firebase(FIREBASE_DB);
        Firebase usersRef = ref.child(child).child(key);
        usersRef.setValue(value, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                if (firebaseError != null) {
                    Log.d(TAG, "Data could not be saved. " + firebaseError.getMessage());
                } else {
                    Log.d(TAG, "Data saved successfully.");
                }
            }
        });
    }

    public ArrayList<Player> getPlayerInfo() {
        return this.playerInfo;
    }

    public boolean isDataFetched() {
        return isDataChanged;
    }


    public void fetchValue(String child) {

        Log.d(TAG, "Get Value for Key - " + child);
        Firebase ref = new Firebase(FIREBASE_DB + "/" + child);
        Query queryRef = ref.orderByChild("score");
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                // snapshot contains the key and value

                isDataChanged = true;
                if (snapshot.getValue() != null) {
                    Log.d(TAG, "Data Received" + snapshot.getValue().toString());

                    // Adding the data to the ArrayList
                    for (DataSnapshot postshot : snapshot.getChildren()) {
                        Player player = postshot.getValue(Player.class);
                        playerInfo.add(player);
                    }
                } else {
                    Log.d(TAG, "Data Not Received");

                }


            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(TAG, firebaseError.getMessage());
                Log.e(TAG, firebaseError.getDetails());
            }
        });
    }
}