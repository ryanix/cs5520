package edu.neu.madcourse.xingxingliu.memowords;

/**
 * Created by cao on 4/10/16.
 */
public class EntryItem {
    private String image;
    private String title;

    public EntryItem() {
        super();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}