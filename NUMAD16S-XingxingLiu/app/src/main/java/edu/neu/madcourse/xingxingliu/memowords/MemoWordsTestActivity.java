package edu.neu.madcourse.xingxingliu.memowords;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.ibm.watson.developer_cloud.tone_analyzer.v3.ToneAnalyzer;
import com.ibm.watson.developer_cloud.tone_analyzer.v3.model.ToneAnalysis;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by cao on 4/17/16.
 */
public class MemoWordsTestActivity extends Activity {
    private AsyncTask<Void, Void, String> asyncTask;
    private static String USER_NAME_TONE = "97a14aca-9c39-4024-bf1b-bc344fa9c932";
    private static String PASSWORD_TONE = "kMxl1iH5YTh7";
    private static String TEST_STRING = "This is just what I need! My parents are getting on my case about doing more extracurricular activities, I have a huge paper due for AP English soon, and I can’t understand a thing in advanced Spanish! The last thing I need is for my best friend to think I hate her and barely text me back anymore.  ";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.memo_test_activity);

        // Call the service and get the tone

        asyncTask = new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... Void) {
                ToneAnalyzer service = new ToneAnalyzer(ToneAnalyzer.VERSION_DATE_2016_02_11);
                service.setUsernameAndPassword(USER_NAME_TONE, PASSWORD_TONE);
                ToneAnalysis tone = service.getTone(TEST_STRING);
                return tone.toString();
            }

            @Override
            protected void onPostExecute(String result) {
                Log.d("This is tone", result);
            }
        };
        asyncTask.execute(null, null, null);

    }
}
