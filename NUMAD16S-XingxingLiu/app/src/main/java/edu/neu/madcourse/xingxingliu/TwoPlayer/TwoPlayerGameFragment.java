package edu.neu.madcourse.xingxingliu.TwoPlayer;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import edu.neu.madcourse.xingxingliu.Scraggle.WordTile;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.MyDatabase;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by Ryanix on 3/21/16.
 */
public class TwoPlayerGameFragment extends Fragment {
    static private int mLargeIds[] = {R.id.large01, R.id.large02, R.id.large03,
            R.id.large04, R.id.large05, R.id.large06, R.id.large07, R.id.large08,
            R.id.large09,};
    static private int mSmallIds[] = {R.id.small01, R.id.small02, R.id.small03,
            R.id.small04, R.id.small05, R.id.small06, R.id.small07, R.id.small08,
            R.id.small09,};
    private Handler mHandler = new Handler();
    private WordTile mEntireBoard = new WordTile(this);
    private WordTile mLargeWordTiles[] = new WordTile[9];
    private WordTile mSmallWordTiles[][] = new WordTile[9][9];

    private int mSoundX, mSoundO, mSoundMiss, mSoundRewind;
    private SoundPool mSoundPool;
    private float mVolume = 1f;
    private SQLiteDatabase db;

    private Set<WordTile> mAvailable = new HashSet<WordTile>();
    private Set<WordTile> mPressed = new HashSet<WordTile>();
    private Set<String> validWordList1 = new HashSet<String>();
    private String word = "";

    private MyDatabase myDatabase;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        myDatabase=new MyDatabase(getActivity());
        db=myDatabase.openDatabase();
        initGame();
        mSoundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        mSoundX = mSoundPool.load(getActivity(), R.raw.sergenious_moveo, 1);
        mSoundO = mSoundPool.load(getActivity(), R.raw.sergenious_movex, 1);
        mSoundMiss = mSoundPool.load(getActivity(), R.raw.erkanozan_miss, 1);
        mSoundRewind = mSoundPool.load(getActivity(), R.raw.joanne_rewind, 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView =
                inflater.inflate(R.layout.word_large_board, container, false);

        initViews(rootView);

        return rootView;
    }

    private Boolean checkWords(String words) {
        Cursor cursor = db.rawQuery("select locale from android_metadata where locale =?;", new String[]{words});
        return cursor.moveToFirst();
    }

    public void setViewText(String[] words) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                TextView tile = (TextView) mSmallWordTiles[i][j].getView();
                tile.setText(words[i].charAt(j) + "");
            }
        }
    }

    private void initViews(View rootView) {
        mEntireBoard.setView(rootView);
        for (int large = 0; large < 9; large++) {
            final int fLarge = large;
            View outer = rootView.findViewById(mLargeIds[large]);
            mLargeWordTiles[large].setView(outer);
            for (int small = 0; small < 9; small++) {
                final TextView inner = (TextView) outer.findViewById
                        (mSmallIds[small]);
                final int fSmall = small;
                final WordTile smallWordTile = mSmallWordTiles[large][small];
                smallWordTile.setView(inner);
                inner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isAvailable(smallWordTile)) {
                            word = word + inner.getText().toString();
                            mPressed.add(smallWordTile);
                            inner.setBackgroundColor(Color.parseColor("#D8B3B3"));
                            mSoundPool.play(mSoundX, mVolume, mVolume, 1, 0, 1f);
                            mAvailable.clear();
                            updateWordTile();
                            ((TwoPlayerGameActivity) getActivity()).sendMessage(fLarge, fSmall);
                        } else {
                            mSoundPool.play(mSoundMiss, mVolume, mVolume, 1, 0, 1f);
                        }
                    }
                });
            }
        }
    }

    public void updateAvailable(int large, int small) {
        mPressed.add(mSmallWordTiles[large][small]);
        TextView v = (TextView) mSmallWordTiles[large][small].getView();
        String w = v.getText().toString();
        v.setBackgroundColor(Color.parseColor("#D8B3B3"));
        word = word + w;
        int[] neighbour = new int[]{};
        switch (large) {
            case (0):
                neighbour = new int[]{1, 4, 3};
                break;
            case (1):
                neighbour = new int[]{0, 2, 3, 4, 5};
                break;
            case (2):
                neighbour = new int[]{1, 5, 4};
                break;
            case (3):
                neighbour = new int[]{0, 1, 4, 6, 7};
                break;
            case (4):
                neighbour = new int[]{0, 1, 2, 3, 5, 6, 7, 8};
                break;
            case (5):
                neighbour = new int[]{1, 2, 4, 7, 8};
                break;
            case (6):
                neighbour = new int[]{3, 4, 7};
                break;
            case (7):
                neighbour = new int[]{3, 4, 5, 6, 8};
                break;
            case (8):
                neighbour = new int[]{4, 5, 7};
                break;
        }
        mAvailable.clear();
        int length = neighbour.length;
        while (length > 0) {
            int next = neighbour[length - 1];
            for (int i = 0; i < 9; i++) {
                WordTile wt = mSmallWordTiles[next][i];
                if (!mPressed.contains(wt)) {
                    mAvailable.add(wt);
                }
            }
            length--;
        }
    }

    public void updateWordTile() {
        if (word.length() >= 3) {
            if (!validWordList1.contains(word)) {
                if (checkWords(word)) {
                    validWordList1.add(word);
                    ((TwoPlayerGameActivity) getActivity()).showValidWord(word);
                    resetWordTile();
                }
            }
        }
    }

    public void resetWordTile() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                mSmallWordTiles[i][j].getView().setBackgroundColor(getResources().getColor(R.color.yell_color));
            }
        }
        mPressed.clear();
        mAvailable.clear();
        word = "";
        setAllAvailable();
    }

    public void initGame() {
        Log.d("UT3", "init game");
        mEntireBoard = new WordTile(this);
        // Create all the WordTiles
        for (int large = 0; large < 9; large++) {
            mLargeWordTiles[large] = new WordTile(this);
            for (int small = 0; small < 9; small++) {
                mSmallWordTiles[large][small] = new WordTile(this);
            }
        }
        setAllAvailable();
    }

    private void setAllAvailable() {
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                WordTile WordTile = mSmallWordTiles[large][small];
                mAvailable.add(WordTile);
            }
        }
    }

    public boolean isAvailable(WordTile WordTile) {
        return mAvailable.contains(WordTile);
    }

    public int computeScores() {
        int score = 0;
        int scorePower = 30;
        Iterator iterator = validWordList1.iterator();
        while (iterator.hasNext()) {
            String word = iterator.next().toString();
            score = score + scorePower * word.length();
        }
        return score;
    }

    /**
     * Create a string containing the state of the game.
     */
    public String getState() {
        StringBuilder builder = new StringBuilder();
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                if (mPressed.contains(mSmallWordTiles[large][small])) {

                    builder.append(large);
                    builder.append(',');
                    builder.append(small);
                    builder.append(',');
                }
            }
        }
        builder.append("&");
        for (int large = 0; large < 9; large++) {
            for (int small = 0; small < 9; small++) {
                if (mAvailable.contains(mSmallWordTiles[large][small])) {

                    builder.append(large);
                    builder.append(',');
                    builder.append(small);
                    builder.append(',');
                }
            }
        }
        builder.append("&");
        Iterator iterator = validWordList1.iterator();
        while (iterator.hasNext()) {
            String word = (String) iterator.next();
            builder.append(word);
            builder.append(",");
        }
        return builder.toString();
    }

    /**
     * Restore the state of the game from the given string.
     */
    public void putState(String gameData) {
        String[] fields = gameData.split("&");
        String pressed = fields[0];
        String available = fields[1];
        String word = fields[2];
        restorePressed(pressed);
        restoreAvailable(available);
        restoreWord(word);
    }

    public void restorePressed(String s) {
        String[] pressed = s.split(",");
        for (int i = 0; i < pressed.length; i = i + 2) {
            int large = Integer.valueOf(pressed[i]);
            int small = Integer.valueOf(pressed[i + 1]);
            mPressed.add(mSmallWordTiles[large][small]);
        }
    }

    public void restoreAvailable(String s) {
        String[] pressed = s.split(",");
        for (int i = 0; i < pressed.length; i = i + 2) {
            int large = Integer.valueOf(pressed[i]);
            int small = Integer.valueOf(pressed[i + 1]);
            mAvailable.add(mSmallWordTiles[large][small]);
        }
    }

    public void restoreWord(String s) {
        String[] pressed = s.split(",");
        for (int i = 0; i < pressed.length; i++) {
            String reword = pressed[i];
            validWordList1.add(reword);
        }
    }

}
