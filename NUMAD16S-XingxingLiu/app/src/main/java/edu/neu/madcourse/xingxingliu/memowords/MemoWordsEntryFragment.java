package edu.neu.madcourse.xingxingliu.memowords;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.ibm.watson.developer_cloud.concept_insights.v2.ConceptInsights;
import com.ibm.watson.developer_cloud.concept_insights.v2.model.Annotations;
import com.ibm.watson.developer_cloud.concept_insights.v2.model.Graph;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by cao on 4/10/16.
 */
public class MemoWordsEntryFragment extends Fragment {
    private AsyncTask<Void, Void, String> asyncTask;
    private AsyncTask<Void, Void, String> asyncTaskTone;
    private static String USER_NAME = "7d6097e5-acb4-4c86-a741-8d72494f52e6";
    private static String PASSWORD = "Op7TPJbJGQaA";
    private static String USER_NAME_TONE = "97a14aca-9c39-4024-bf1b-bc344fa9c932";
    private static String PASSWORD_TONE = "kMxl1iH5YTh7";
    private String TAG = "MemoWordsEntryFragment";
    private static String FIREBASE_URL = "https://memoword.firebaseio.com";
    private GridView mGridView;
    private GridViewAdapter mGridAdapter;
    private TextView speechTextView;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private Record testEntry1 = new Record();
    private Record testEntry2 = new Record();
    private Record testEntry3 = new Record();
    private Record testEntry4 = new Record();
    private Record testEntry5 = new Record();
    private Record testEntry6 = new Record();
    public ArrayList<Record> mGridData;

    //private fields for creating record
    private String address = "";
    private String time = "";
    private String preview = "";
    private String template = "";
    private byte[] record;

    private SwipeRefreshLayout swipeRefreshLayout;
    MediaPlayer mediaPlayer = new MediaPlayer();

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(MemoWordsShareFragment.FROM_SHARE_FRAGMENT)) {
                String templateTitle = intent.getStringExtra("DOWNLOAD");


                Log.d(TAG, "Title from share fragment " + templateTitle);
                Boolean checkup = new VoiceMemoDatabase(getActivity()).checkDuplicateTemplate(templateTitle);
                if (!checkup) {
                    new VoiceMemoDatabase(getActivity()).addTemplate(templateTitle);
                    Record record = new Record();
                    record.setTemplate(templateTitle);
                    for (Record data : mGridData) {
                        if (templateTitle.equals(data.getTemplate())) {
                            return;
                        }
                    }
                    mGridData.add(record);
                }
                else
                    Toast.makeText(getActivity(),"Template already exists!",Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(getContext());
        IntentFilter intentFilter = new IntentFilter(MemoWordsShareFragment.FROM_SHARE_FRAGMENT);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.memo_entry_fragment, container, false);
        // Initialize views
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.memo_entry_fragment);


        mGridView = (GridView) rootView.findViewById(R.id.memo_entry_gridview);
        // Initialize grid data
        mGridData = new ArrayList<>();
        initEntryTemplate();
        mGridData.add(testEntry1);
        mGridData.add(testEntry2);
        mGridData.add(testEntry3);
        mGridData.add(testEntry4);
        mGridData.add(testEntry5);
        mGridData.add(testEntry6);
        mGridAdapter = new GridViewAdapter(getContext(), R.layout.memo_grid_item, mGridData);
        mGridView.setAdapter(mGridAdapter);

        // Set click listeners for grid view

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Record item = (Record) parent.getItemAtPosition(position);
                //View rootView = mGridView.findViewById(R.id.grid_item_layout);
                View frontView = view.findViewById(R.id.grid_item_template_layout);
                View backView = view.findViewById(R.id.grid_item_record_layout);

                FlipAnimation flipAnimation = new FlipAnimation(frontView, backView);
                if (frontView.getVisibility() == View.GONE) {
                    flipAnimation.reverse();
                }
                view.startAnimation(flipAnimation);


                // Need to register a data lisener here to accept the audio data

                Log.d(TAG, "Inside click listener!");
                Log.d(TAG, "Size is " + mGridView.getChildCount());
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initEntryTemplate();
                mGridData.clear();
                mGridAdapter.clear();
                mGridData.add(testEntry1);
                mGridData.add(testEntry2);
                mGridData.add(testEntry3);
                mGridData.add(testEntry4);
                mGridData.add(testEntry5);
                mGridData.add(testEntry6);
                mGridAdapter = new GridViewAdapter(getContext(), R.layout.memo_grid_item, mGridData);
                mGridView.setAdapter(mGridAdapter);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        return rootView;
    }

    public void initEntryTemplate(){
        List<String> template = new VoiceMemoDatabase(getActivity()).pickTemplates();
        testEntry1.setTemplate(template.get(0));
        testEntry2.setTemplate(template.get(1));
        testEntry3.setTemplate(template.get(2));
        testEntry4.setTemplate(template.get(3));
        testEntry5.setTemplate(template.get(4));
        testEntry6.setTemplate(template.get(5));
    }
    /**
     * Showing google speech input dialog
     */
    private void promptSpeechInput() {

        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra("android.speech.extra.GET_AUDIO_FORMAT", "audio/AMR");
        intent.putExtra("android.speech.extra.GET_AUDIO", true);


        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.speech_prompt));

        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getContext(),
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Receiving speech input
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        final ArrayList<String> resultData;
        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (data != null) {
                    //TextView textView = (TextView) mGridView.findViewById(R.id.grid_item_record_text);
                    resultData = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    Log.d("TAG", "Result: " + resultData);


                    // extract audio from (Intent) data
                    Uri audioUri = data.getData();
                    
                    ContentResolver contentResolver = getActivity().getContentResolver();
                    //convert audio into byte array
                    try{
                        InputStream fileStream = contentResolver.openInputStream(audioUri);
                        record = getBytes(fileStream);
                        fileStream.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    asyncTask = new AsyncTask<Void, Void, String>() {

                        @Override
                        protected String doInBackground(Void... Void) {
                            Annotations annotations;
                            if (isNetworkAvailable()){
                                ConceptInsights service = new ConceptInsights();
                                service.setUsernameAndPassword(USER_NAME, PASSWORD);

                                annotations =
                                        service.annotateText(Graph.WIKIPEDIA, resultData.get(0));
                            } else {
                                return resultData.get(0);
                            }

                            return annotations.toString();
                        }

                        @Override
                        protected void onPostExecute(String result) {
                            try {
                                if (!isNetworkAvailable()){
                                    preview = result;
                                    DateFormat df = new SimpleDateFormat("MM/dd/yy");
                                    time = df.format(new Date());
                                    Record newRecord = new Record(time,address,template,record,preview);
                                    new VoiceMemoDatabase(getActivity()).addRecord(newRecord);
                                } else {
                                    StringBuilder sb = new StringBuilder();
                                    final JSONObject obj = new JSONObject(result);
                                    final JSONArray array = obj.getJSONArray("annotations");
                                    int n = array.length();
                                    if (n > 3){
                                        n = 3;
                                    }
                                    for (int i = 0; i < n; i++){
                                        final JSONObject annotation = array.getJSONObject(i);
                                        Log.d(TAG, annotation.toString());
                                        String label = annotation.getJSONObject("concept").getString("label");
                                        Log.d(TAG, label);
                                        sb.append(label).append("\n");
                                    }
                                    if (sb.toString().isEmpty()){
                                        preview = resultData.get(0);
                                    } else {
                                        preview = sb.toString();
                                    }
                                    // store the record into database
                                    //preview = result.get(0);
                                    DateFormat df = new SimpleDateFormat("MM/dd/yy");
                                    time = df.format(new Date());
                                    Record newRecord = new Record(time,address,template,record,preview);
                                    new VoiceMemoDatabase(getActivity()).addRecord(newRecord);
                                }
                            } catch (JSONException e){
                                e.printStackTrace();
                            }
                            Log.d("This is key word", result);
                        }
                    };
                    asyncTask.execute(null, null, null);

//                    asyncTaskTone = new AsyncTask<Void, Void, String>() {
//
//                        @Override
//                        protected String doInBackground(Void... Void) {
//                            ToneAnalyzer service = new ToneAnalyzer(ToneAnalyzer.VERSION_DATE_2016_02_11);
//                            service.setUsernameAndPassword(USER_NAME_TONE, PASSWORD_TONE);
//                            ToneAnalysis tone = service.getTone(result.get(0));
//                            return tone.toString();
//                        }
//
//                        @Override
//                        protected void onPostExecute(String result) {
//                            try {
//                                StringBuilder sb = new StringBuilder();
//                                final JSONObject obj = new JSONObject(result);
//                                final JSONArray array = obj.getJSONArray("");
//                                int n = array.length();
//                                if (n > 3){
//                                    n = 3;
//                                }
//                                for (int i = 0; i < n; i++){
//                                    final JSONObject annotation = array.getJSONObject(i);
//                                    Log.d(TAG, annotation.toString());
//                                    String label = annotation.getJSONObject("concept").getString("label");
//                                    Log.d(TAG, label);
//                                    sb.append(label).append(" ");
//                                }
//                                preview = sb.toString();
//                                // store the record into database
//                                //preview = result.get(0);
//                                DateFormat df = new SimpleDateFormat("MM/dd/yy");
//                                time = df.format(new Date());
//                                Record newRecord = new Record(time,address,template,record,preview);
//                                new VoiceMemoDatabase(getActivity()).addRecord(newRecord);
//                            } catch (JSONException e){
//                                e.printStackTrace();
//                            }
//                            Log.d("This is tone", result);
//                        }
//                    };
//                    asyncTaskTone.execute(null, null, null);
                    // flip into front

                }
                break;
            }
        }
    }

    public void feedBackAfterRecording(String template,String time){
        List<Record> records = new VoiceMemoDatabase(getActivity()).getSimilarRecord(template, time);
        int size = records.size();
        int random;
        final Record record;
        if(size > 0) {
            random = new Random().nextInt(size);
            record = records.get(random);
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Similar Record");
            alert.setMessage("Textual content:\n" + record.preview);
            Button playAudio = new Button(getActivity());
            alert.setPositiveButton("Audio", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    try {
                        // create temp file that will hold byte array
                        File tempMp3 = File.createTempFile("temAudio", "mp3", getActivity().getCacheDir());
                        tempMp3.deleteOnExit();
                        FileOutputStream fos = new FileOutputStream(tempMp3);
                        fos.write(record.record);
                        fos.close();

                        FileInputStream fis = new FileInputStream(tempMp3);
                        mediaPlayer.reset();
                        mediaPlayer.setDataSource(fis.getFD());

                        mediaPlayer.prepare();
                        mediaPlayer.start();
                    } catch (IOException ex) {
                        String s = ex.toString();
                        ex.printStackTrace();
                    }
                }
            });
            alert.setNegativeButton("Preview", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alert.show();
        }
    }
    //convert the audio into byte array to be stored in the database
    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }
    // retrieve street name from location service
    public void getLocation() {
        AppLocationService appLocationService = new AppLocationService(getActivity());
        Location location = appLocationService
                .getLocation(LocationManager.GPS_PROVIDER);

        if (location != null) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            LocationAddress locationAddress = new LocationAddress();
            locationAddress.getAddressFromLocation(latitude, longitude,
                    getActivity(), new GeocoderHandler());
        } else {
            showSettingsAlert();
        }
    }

    //navigate to GPS settings when GPS is not available
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                getActivity());
        alertDialog.setTitle("SETTINGS");
        alertDialog.setMessage("Enable Location Provider! Go to settings menu?");
        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        getActivity().startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }

    //handle longitude and latitude
    private class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }
            address = locationAddress;
        }
    }

    // Inner class for a custom GridViewAdapter
    private class GridViewAdapter extends ArrayAdapter<Record> {
        private Context mContext;
        private int layoutResourceId;
        private ArrayList<Record> mGridData = new ArrayList<>();

        public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<Record> mGridData) {
            super(mContext, layoutResourceId, mGridData);
            this.layoutResourceId = layoutResourceId;
            this.mContext = mContext;
            this.mGridData = mGridData;
        }

        /**
         * Updates grid data and refresh grid items.
         *
         * @param mGridData
         */
        public void setGridData(ArrayList<Record> mGridData) {
            this.mGridData = mGridData;
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View row = convertView;
            final ViewHolder holder;

            if (row == null) {
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                row = inflater.inflate(layoutResourceId, parent, false);
                holder = new ViewHolder();
                Log.d(TAG, row.toString());
                holder.frontView = row.findViewById(R.id.grid_item_template_layout);
                holder.backView = row.findViewById(R.id.grid_item_record_layout);
                holder.templateTextView = (TextView) row.findViewById(R.id.grid_item_template_text);
                holder.playImageView = (ImageView) row.findViewById(R.id.grid_item_play_image);
                holder.backPlayImageView = (ImageView) row.findViewById(R.id.grid_item_record_button);
                holder.backTextView = (TextView) row.findViewById(R.id.grid_item_record_text);
                holder.previewTextView = (ImageView) row.findViewById(R.id.grid_item_preview_button);
                holder.emptyView = row.findViewById(R.id.grid_item_empty_view);
                row.setTag(holder);
            } else {
                holder = (ViewHolder) row.getTag();
            }

            final Record item = mGridData.get(position);
            holder.templateTextView.setText(Html.fromHtml(item.getTemplate()));
            holder.backTextView.setText("Click icon to begin");

            // Set click listeners
            holder.emptyView.setOnClickListener(null);
            holder.backPlayImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    template = item.template;
                    promptSpeechInput();
                    FlipAnimation flipAnimation = new FlipAnimation(holder.frontView, holder.backView);
                    if (holder.frontView.getVisibility() == View.GONE) {
                        flipAnimation.reverse();
                    }
                    convertView.startAnimation(flipAnimation);
                }
            });

            holder.playImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Play the audio
                    List<Record> records = new VoiceMemoDatabase(getActivity()).getSimilarRecord(template, time);
                    if (!records.isEmpty()) {
                        byte[] audioByte = records.get(0).record;
                        playMp3(audioByte);
                    }
                }
            });

            holder.previewTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Inside preview clicking listeners!");
                    template = item.template;
                    final Record record = item;
                    final PopupMenu popupMenu = new PopupMenu(getContext(), v);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_item, popupMenu.getMenu());
                    popupMenu.show();
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_preview:
                                    retrieveAudioText(position);
                                    return true;
                                case R.id.action_edit:
                                    editTemplate(record);
                                    return true;
                                case R.id.action_share:
                                    shareToFirebase(holder.templateTextView.getText().toString());
                                    return true;
                            }
                            return false;
                        }
                    });

                }
            });
            return row;
        }

        // edit current template
        public void editTemplate(final Record record){
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            final EditText editText = new EditText(getActivity());
            alert.setMessage("Type in your own template");
            alert.setTitle("Customized Template");
            alert.setView(editText);

            alert.setPositiveButton("Complete Composing", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String editable = editText.getText().toString();
                    Boolean checkTemplate = new VoiceMemoDatabase(getActivity()).checkDuplicateTemplate(editable);
                    if (!checkTemplate) {
                        new VoiceMemoDatabase(getActivity()).addTemplate(editable);
                        record.template = editable;
                    } else {
                        Toast.makeText(getActivity(), "Template already exists.", Toast.LENGTH_LONG).show();
                        editText.setText("");
                    }
                }
            });
            alert.setNegativeButton("Cancel", null);
            alert.show();
        }
        // play audio from byte array
        private void playMp3(byte[] mp3SoundByteArray) {
            try {
                // create temp file that will hold byte array
                File tempMp3 = File.createTempFile("temAudio", "mp3", getActivity().getCacheDir());
                tempMp3.deleteOnExit();
                FileOutputStream fos = new FileOutputStream(tempMp3);
                fos.write(mp3SoundByteArray);
                fos.close();

                FileInputStream fis = new FileInputStream(tempMp3);
                mediaPlayer.reset();
                mediaPlayer.setDataSource(fis.getFD());

                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException ex) {
                String s = ex.toString();
                ex.printStackTrace();
            }
        }

        private void retrieveAudioText(int position){
            //retrieve record from database with same template and time
            DateFormat df = new SimpleDateFormat("MM/dd/yy");
            time = df.format(new Date());
            template = mGridData.get(position).template;
            List<Record> records = new VoiceMemoDatabase(getActivity()).getSimilarRecord(template, time);
            String databaseResult="";
            if(!records.isEmpty()){
                databaseResult = databaseResult + records.get(0).preview;
                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                dialog.setMessage(databaseResult);
                dialog.setTitle("Text Preview");
                dialog.setPositiveButton("OK",null);
                dialog.show();
            }
        }
        private final class ViewHolder {
            TextView templateTextView;
            ImageView playImageView;
            ImageView backPlayImageView;
            TextView backTextView;
            ImageView previewTextView;
            View frontView;
            View backView;
            View emptyView;
        }

    }
    private void shareToFirebase(String template){
        final Firebase myFirebaseRef = new Firebase(FIREBASE_URL);
        myFirebaseRef.child(template).setValue(template);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    // Check if the user can get data from Firebase
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}





