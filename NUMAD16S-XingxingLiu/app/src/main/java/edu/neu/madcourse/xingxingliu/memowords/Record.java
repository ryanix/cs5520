package edu.neu.madcourse.xingxingliu.memowords;


/**
 * Created by Ryanix on 4/10/16.
 */
public class Record {
    String recordTime;
    String recordLocation;
    String template;
    byte[] record;
    String preview;

    public Record(){

    }
    public Record(String time,String location,String template,byte[] record,String preview){
        this.recordTime = time;
        this.recordLocation = location;
        this.template = template;
        this.record = record;
        this.preview = preview;
    }

    public String getTemplate() {
        return template;
    }

    public String getRecordTime(){
        return recordTime;
    }

    public String getRecordLocation(){
        return recordLocation;
    }

    public String getPreview(){
        return preview;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public void setRecordTime(String recordTime){
        this.recordTime = recordTime;
    }

    public void setRecordLocation(String recordLocation){
        this.recordLocation = recordLocation;
    }
    public void setPreview(String preview){
        this.preview = preview;
    }

}
