package edu.neu.madcourse.xingxingliu.numad16s_xingxingliu;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

public class MyDatabase extends SQLiteOpenHelper {

    private static final int DB_VERSION=1;
    private final Context myContext;
    private SQLiteDatabase myDatabase;

    public MyDatabase(Context context){
        super(context, "wordList.db", null, DB_VERSION);
        this.myContext=context;

    }


    public void createDatebase() throws IOException{
        boolean dbExist = checkDatabase();

        if(dbExist) {
            this.getWritableDatabase();
        }
        dbExist=checkDatabase();
        if(!dbExist){
            this.getReadableDatabase();
        }
            try {
                copyDatabase();

            } catch (IOException e) {
                throw new Error(e.toString());
            }

        }


    private boolean checkDatabase() {
        File dbFile = myContext.getDatabasePath("wordList.db");
        return dbFile.exists();
    }
    private void copyDatabase() throws IOException{
        Log.d("xxxxxx",myContext.getAssets().list("/").toString());
        InputStream myInput= myContext.getResources().getAssets().open("wordList.db");
        File f = new File(myContext.getDatabasePath("wordList.db").getPath());
        OutputStream myOutput = new FileOutputStream(f);
        int size = myInput.available();
        byte[] buffer = new byte[size];
        int length;
        while((length = myInput.read(buffer))>0){
            myOutput.write(buffer,0,length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    public SQLiteDatabase openDatabase(){

       myDatabase=SQLiteDatabase.openDatabase(
               myContext.getDatabasePath("wordList.db").getPath(),
               null,
               SQLiteDatabase.OPEN_READONLY);

        return myDatabase;

    }
    @Override
    public void onCreate(SQLiteDatabase db){

    }

    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion) {

    }


}
