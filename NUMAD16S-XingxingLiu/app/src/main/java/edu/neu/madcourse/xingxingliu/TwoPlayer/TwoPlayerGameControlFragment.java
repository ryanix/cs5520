package edu.neu.madcourse.xingxingliu.TwoPlayer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

/**
 * Created by Ryanix on 3/21/16.
 */
public class TwoPlayerGameControlFragment extends Fragment{


        private AlertDialog mDialog;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView =
                    inflater.inflate(R.layout.online_fragment_word_control, container, false);
            View soundButton = rootView.findViewById(R.id.online_button_sound);
            View appAcknowledgement = rootView.findViewById(R.id.online_acknowledgements_button);
            View quit = rootView.findViewById(R.id.online_quit_button);

            quit.setOnClickListener(new View.OnClickListener(){
                @Override
            public  void onClick(View view){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("QUIT");
                    builder.setMessage("Quit will lose all the score you've earn, are you sure?");
                    builder.setCancelable(true);
                    builder.setPositiveButton(R.string.ok_label,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ((TwoPlayerGameActivity)getActivity()).finishGame();
                                }
                            });
                    mDialog=builder.show();
                }
            });

            soundButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((TwoPlayerGameActivity)getActivity()).musicControl();
                }
            });

            appAcknowledgement.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.dictionary_acknowledgements);
                    builder.setMessage(R.string.acknowledgements);
                    builder.setCancelable(false);
                    builder.setPositiveButton(R.string.ok_label,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                }
                            });
                    mDialog=builder.show();

                }
            });
            return rootView;
        }

    }


