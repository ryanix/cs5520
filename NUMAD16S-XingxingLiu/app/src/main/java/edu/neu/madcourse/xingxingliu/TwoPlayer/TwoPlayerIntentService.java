package edu.neu.madcourse.xingxingliu.TwoPlayer;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import edu.neu.madcourse.xingxingliu.Communication.CommunicationMessage;
import edu.neu.madcourse.xingxingliu.Communication.GcmBroadcastReceiver;
import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;

public class TwoPlayerIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;

    private String teammateId;
    private String teammateName;
    private String myId;
    private String myName;
    private String myScore;
    private String vsScore;
    private String moveOnX;
    private String moveOnY;
    public TwoPlayerIntentService() {
        super("TwoPlayerIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        myId = extras.getString("vsId");
        myName = extras.getString("vsName");
        teammateId = extras.getString("id");
        teammateName = extras.getString("name");
        myScore = extras.getString("vsScore");
        vsScore = extras.getString("myScore");
        moveOnX = extras.getString("large");
        moveOnY = extras.getString("small");

        if (!extras.isEmpty()) {
            int i = Integer.valueOf(moveOnX) + 1;
            int j = Integer.valueOf(moveOnY) + 1;
            String message = teammateName + "made a new move on : \n"
                    + "large board on row " + (i/3 + 1)+" and column " +i%3 + "\n"
                    + "small board on row " + (j/3 + 1) +" and column " +j%3 + "\n";
            sendNotification(message);
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        TwoPlayerReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    public void sendNotification(String message) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent;
        notificationIntent = new Intent(this, TwoPlayerGameActivity.class);
        notificationIntent.putExtra("myId",myId);
        notificationIntent.putExtra("myName",myName);
        notificationIntent.putExtra("myScore",myScore);
        notificationIntent.putExtra("vsId",teammateId);
        notificationIntent.putExtra("vsName",teammateName);
        notificationIntent.putExtra("vsScore", vsScore);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Teammate has made a new move.")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message).setTicker(message)
                .setAutoCancel(true);
        mBuilder.setContentIntent(intent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}