package edu.neu.madcourse.xingxingliu.Communication;

/**
 * Created by Ryanix on 3/10/16.
 */
public class Player {
    private String username;
    private String id;
    private Integer score;

    public Player() {
    }

    public Player(String username, String id, Integer score) {
        this.username = username;
        this.id = id;
        this.score = score;
    }

    public String getUsername() {
        return this.username;
    }

    public String getId() {
        return this.id;
    }

    public Integer getScore() {
        return this.score;
    }
}
