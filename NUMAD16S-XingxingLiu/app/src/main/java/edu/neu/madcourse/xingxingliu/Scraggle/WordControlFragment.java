package edu.neu.madcourse.xingxingliu.Scraggle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.neu.madcourse.xingxingliu.numad16s_xingxingliu.R;


public class WordControlFragment extends Fragment {

   private AlertDialog mDialog;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
      View rootView =
            inflater.inflate(R.layout.fragment_word_control, container, false);
      View newButton = rootView.findViewById(R.id.button_new);
      View soundButton = rootView.findViewById(R.id.button_sound);
      final View pauseButton = rootView.findViewById(R.id.word_pause_button);
      final View resumeButton = rootView.findViewById(R.id.word_resume_button);
      View quitButton=rootView.findViewById(R.id.button_about);
      View appAcknowledgement = rootView.findViewById(R.id.acknowledgements_button);


      pauseButton.setVisibility(View.VISIBLE);
      resumeButton.setVisibility(View.INVISIBLE);

      pauseButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            ((WordGameActivity) getActivity()).cancelTimer();
            ((WordGameActivity) getActivity()).setBoardVisibility(View.INVISIBLE);
            pauseButton.setVisibility(View.INVISIBLE);
            resumeButton.setVisibility(View.VISIBLE);
         }
      });
      resumeButton.setOnClickListener(new View.OnClickListener(){
         @Override
         public void onClick(View view){
            ((WordGameActivity)getActivity()).setTimer();
            ((WordGameActivity)getActivity()).setBoardVisibility(View.VISIBLE);
            pauseButton.setVisibility(View.VISIBLE);
            resumeButton.setVisibility(View.INVISIBLE);
         }
      });
      soundButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            ((WordGameActivity)getActivity()).musicControl();
         }
      });
      newButton.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            Intent intent =new Intent(getActivity(),WordGameActivity.class);
            getActivity().startActivity(intent);
            getActivity().finish();
         }
      });
      appAcknowledgement.setOnClickListener(new View.OnClickListener(){
         @Override
         public void onClick(View view){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.dictionary_acknowledgements);
            builder.setMessage(R.string.acknowledgements);
            builder.setCancelable(false);
            builder.setPositiveButton(R.string.ok_label,
                    new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialogInterface, int i) {
                       }
                    });
            mDialog=builder.show();

         }
      });
      quitButton.setOnClickListener(new View.OnClickListener(){
         @Override
         public void onClick(View view){

           getActivity().finish();
         }
      });
      return rootView;
}

}
