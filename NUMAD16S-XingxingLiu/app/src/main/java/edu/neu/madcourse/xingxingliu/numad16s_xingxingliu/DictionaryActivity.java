package edu.neu.madcourse.xingxingliu.numad16s_xingxingliu;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

public class DictionaryActivity extends AppCompatActivity{


    private AlertDialog mDialog;
    private MyDatabase myDatabase;
    public String word_being_looked_up;
    private ArrayList<String> results=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myDatabase=new MyDatabase(this);
        getSupportActionBar().setTitle(R.string.dictionary_title);
        setContentView(R.layout.app_dictionary);

        View clearButton = findViewById(R.id.dictionary_clear_button);
        View aknowlegementsButton =findViewById(R.id.dictionary_acknowledgements_button);
        View returnToMenuButton = findViewById(R.id.dictionary_returnToMenu_button);
        final EditText editText = (EditText)findViewById(R.id.dictionary_entry);
        final TextView dictionaryResult = (TextView)findViewById(R.id.empty_result);
        dictionaryResult.setText(R.string.empty_result);

        editText.setHint("Enter word here");
        final MediaPlayer mMediaPlayer = MediaPlayer.create(DictionaryActivity.this, R.raw.sergenious_movex);
        mMediaPlayer.setVolume(0.5f, 0.5f);
        mMediaPlayer.setLooping(false);

        final SQLiteDatabase db=myDatabase.openDatabase();


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                word_being_looked_up = s.toString();

                if (word_being_looked_up.length() >= 3) {


               Cursor cursor = db.rawQuery("select locale from android_metadata where locale =?;", new String[]{word_being_looked_up});
                    while (cursor.moveToNext()) {
                        results.add(cursor.getString(0));
                    }
                    cursor.close();
                    if (results.contains(word_being_looked_up)) {
                        mMediaPlayer.start();
                        dictionaryResult.setText(
                                results.toString()
                                        .replace(",", "\n")
                                        .replace("[", "")
                                        .replace("]", "")
                                        .trim());
                    }
                }
            }

            });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText(null);
                results.clear();
                dictionaryResult.setText(R.string.empty_result);
            }
        });

        aknowlegementsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                AlertDialog.Builder builder = new AlertDialog.Builder(DictionaryActivity.this);
                builder.setTitle(R.string.dictionary_acknowledgements);
                builder.setMessage(R.string.acknowledgements);
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.ok_label,
                        new DialogInterface.OnClickListener() {
                            @Override
                        public void onClick(DialogInterface dialogInterface,int i){}
                        });
                mDialog=builder.show();

            }
        });

        returnToMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        if(mDialog!=null)
            mDialog.dismiss();
    }
    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
